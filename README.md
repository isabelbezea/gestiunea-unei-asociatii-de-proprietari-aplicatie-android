I am currently working on this project and uploading it here regularly, so I will present what  I've done so far until now.


The application is Android based and it is using Firebase Cloud for storing my data. The first step that the user encounters is the LogIn one. Here, the user existence is always checked in database. Otherwise, he has to create an account. In the proccess of creating account, he also has to answer to a security question of his choice from a list of predefined questions. That way, in case that the password is forgotten, he can change it based on his initial answer to the security question.

![](images/login.PNG)

The steps of password recovery are presented in the second image. The fields are displayed  progressive, after inputs validation.

![](images/steps.PNG)


The third image displays the dashboard of the application from the User's Perspective, and also from the Administrator's Perspective. Both of them have different functionalities. 

![](images/dashboard2.png)

The first option he has is the introduction of the indexes. To simplify the proccess, he can do that either manually, or by taking pics (the algorithm will read the image and autocomplete the boxes).

![](images/introduceree.PNG)

The second option is listing the bills he has to pay, from where he can either pay them (if the bills are not paid yet; otherwise, the button will be hidden), or download them in PDF format.
It also informs the user regarding the outstanding payments. In order to pay the current bill, the previous ones has to be paid (otherwise, the payment is not available).

![](images/bills.png)![](images/pdf.png)

The payment proccess is shown in the picture below and  it provides real time validation for the fields. 

![](images/pay.png)

The third option presented refers to the storage of Official Documents.  Only the administrator is allowed to upload documents, which are stored in **Firebase Storage** and are visible to all users by downloading them.

![](images/documents.png)![](images/categories.png)


The administrator has also the right to post on  the notification board, in order to inform the users about new important events that are happening in the community.

![](images/news2.png)

The application provides the possibility to find/add contacts of people that are tasked with the maintenance of the building, as well as view the details of any residence (usable area, if he is has or not a thermal power plant etc.) which are required in the proccess of issuing invoices. The administrator has also the right to view the bills of a specific  user.

![](images/agenda.png)![](images/managing.png)

The user is tasked with the completion of these details, first time when using the app. 

![](images/alertdialog.png)![](images/meet.png)

The user can view different charts regarding its consumption level and other useful information.

![](images/statistics.png)









