package com.example.licenta;

import android.os.Bundle;
import android.util.Log;
import android.util.Patterns;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.viewpager.widget.ViewPager;

import com.example.licenta.asyncTask.Callback;
import com.example.licenta.firebase.FirebaseService;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;

public class SigninTabFragment extends Fragment {

    EditText etUsername;
    EditText etPassword;
    EditText etEmail;
    EditText etNrApartament;
    TextView tvNrPersoane;
    Spinner spnNrPersoane;

    Spinner spnIntrebareSecuritate;
    EditText etRaspunsIntrebareSecuritate;
    Button btnCreeaza;


    int nrPersoaneSelectat;

    String intrebareSecuritateSelectata;

    //for database
   // private UserService userService;

    private FirebaseService firebaseService;

    private  static ArrayList<User> users_list=new ArrayList<>() ;

    private User user=null;

    float  v=0;


TabLayout tabLayout;
ViewPager viewPager;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup root=(ViewGroup) inflater.inflate(R.layout.signin_tab_fragment, container, false);

        //initViews
        etUsername=root.findViewById(R.id.et_username);
        etPassword=root.findViewById(R.id.et_password);
        etEmail=root.findViewById(R.id.et_email);
        etNrApartament=root.findViewById(R.id.et_nr_apartament);
        tvNrPersoane=root.findViewById(R.id.tv_nr_persoane);
        spnNrPersoane=root.findViewById(R.id.sp_nr_persoane);
        spnIntrebareSecuritate=root.findViewById(R.id.sp_intrebare_securitate);
        etRaspunsIntrebareSecuritate=root.findViewById(R.id.et_raspuns_intrebare);
        btnCreeaza=root.findViewById(R.id.btn_creeaza);


        tabLayout=root.findViewById(R.id.tab_layout);
        viewPager=root.findViewById(R.id.view_pager) ;

        etUsername.setTranslationX(800);
        etPassword.setTranslationX(800);
        etEmail.setTranslationX(800);
        etNrApartament.setTranslationX(800);
        tvNrPersoane.setTranslationX(800);
        spnNrPersoane.setTranslationX(800);
        spnIntrebareSecuritate.setTranslationX(800);
        etRaspunsIntrebareSecuritate.setTranslationX(800);
        btnCreeaza.setTranslationX(800);

        etUsername.setAlpha(v);
        etPassword.setAlpha(v);
        etEmail.setAlpha(v);
        etNrApartament.setAlpha(v);
        tvNrPersoane.setAlpha(v);
        spnNrPersoane.setAlpha(v);
        spnIntrebareSecuritate.setAlpha(v);
        etRaspunsIntrebareSecuritate.setAlpha(v);
        btnCreeaza.setAlpha(v);

        etUsername.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(300).start();
        etPassword.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(300).start();
        etEmail.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(300).start();
        etNrApartament.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(300).start();
        tvNrPersoane.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(300).start();
        spnNrPersoane.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(300).start();
        spnIntrebareSecuritate.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(300).start();
        etRaspunsIntrebareSecuritate.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(300).start();
        btnCreeaza.animate().translationX(0).alpha(1).setDuration(800).setStartDelay(300).start();


        spnNrPersoane.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                nrPersoaneSelectat=Integer.parseInt(parent.getItemAtPosition(position).toString());
                Log.v("pentru noi", nrPersoaneSelectat+".");


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        spnIntrebareSecuritate.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                intrebareSecuritateSelectata=parent.getItemAtPosition(position).toString();
                Log.v("pentru noi", intrebareSecuritateSelectata+".");


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        firebaseService = FirebaseService.getInstance();
        setupListeners();
        return root;
    }

    private boolean validateContent() {
        if (etUsername.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), "Username-ul trebuie introdus", Toast.LENGTH_LONG).show();
            return false;
        }
        if (etPassword.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), "Parola trebuie introdusa", Toast.LENGTH_LONG).show();
            return false;
        }

        if (etEmail.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), "Adresa de mail trebuie introdusa", Toast.LENGTH_LONG).show();
            return false;
        }

        //verificare daca ce am introdus in email are forma specifica
        if (!Patterns.EMAIL_ADDRESS.matcher(etEmail.getText().toString()).matches()) {
            Toast.makeText(getContext(), "Adresa de mail format invalid", Toast.LENGTH_LONG).show();
            return false;
        }

        if (tvNrPersoane.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), "Numarul de persoane care locuiesc in imobil trebuie introdus", Toast.LENGTH_LONG).show();
            return false;
        }


        if (etRaspunsIntrebareSecuritate.getText().toString().isEmpty()) {
            Toast.makeText(getContext(), "Raspunsul la intrebarea de securitate trebuie introdus", Toast.LENGTH_LONG).show();
            return false;
        }
        return true;

    }

    //incarcam butonul cu o actiune
    private void setupListeners(){
        btnCreeaza.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(validateContent()){
                    User user=new User();
                    user.setNumeUtilizator(etUsername.getText().toString());
                    user.setParola(etPassword.getText().toString());
                    user.setEmail(etEmail.getText().toString());
                    user.setNrApartament(Integer.parseInt(etNrApartament.getText().toString()));


                    user.setNrPersoane(nrPersoaneSelectat);
                    user.setIntrebareSecuritate(intrebareSecuritateSelectata);
                    user.setRaspunsIntrebare(etRaspunsIntrebareSecuritate.getText().toString());

//                    userService=new UserService(getContext());
//                    userService.insert(insertIntoDbCallback(), user);

                    firebaseService.upsert(user);


                    Toast.makeText(getActivity(), "User registered!", Toast.LENGTH_SHORT).show();


                    Log.v("pentru noi", user.toString()) ;
                //    tabLayout.getTabAt(1).select();
                   // viewPager.setCurrentItem(0,true);
                    etUsername.setText("");
                    etPassword.setText("");
                    etEmail.setText("");
                    etNrApartament.setText("");
                    etRaspunsIntrebareSecuritate.setText("");






                }
            }
        });
    }

    private Callback<User> insertIntoDbCallback() {
        return new Callback<User>() {
            @Override
            public void runResultOnUiThread(User result) {
                if (result != null) {
                    users_list.add(result);
                    //notifyAdapter();
                }
            }
        };
    }


}
