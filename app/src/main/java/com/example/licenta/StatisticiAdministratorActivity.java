package com.example.licenta;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Spinner;
import android.widget.TextView;

import com.example.licenta.firebase.Callback;
import com.example.licenta.firebase.FirebaseService;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;
import java.util.List;

public class StatisticiAdministratorActivity extends AppCompatActivity {




    TextView tvNrPersoane;
    int count=0 ;

    ArrayList<BarEntry> visitors=new ArrayList<>();
    ArrayList<BarEntry> visitors1=new ArrayList<>();


    public BarChart barChart;
    public BarChart barChart1;


    ArrayList<String> date_facturi=new ArrayList<>();
    ArrayList<Float> sume_facturi=new ArrayList<>();
    ArrayList<Float> mcApa=new ArrayList<>();


    Spinner spnTipGrafic;
    String tipGraficSelectat="Valori in lei";


    BarDataSet barDataSet;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistici_administrator);

        tvNrPersoane=findViewById(R.id.tv_nr_total_persoane);


        barChart=findViewById(R.id.grafic_bareA) ;
        spnTipGrafic=findViewById(R.id.spn_tip_grafic) ;

        barChart1=findViewById(R.id.grafic_bareA2);

        for(User user:DashboardActivity.users_list){

            count=count+user.getNrPersoane();
        }

        tvNrPersoane.setText(count + " persoane");

        for(Factura factura : DashboardActivity.facturi_list){
            if(!date_facturi.contains(factura.getDataScadenta().split("/")[1])){
                date_facturi.add(factura.getDataScadenta().split("/")[1]);
            }
        }

        for(String data:date_facturi){
            float sum=0; float sum1=0;
            for(Factura factura:DashboardActivity.facturi_list){
                if(factura.getDataScadenta().split("/")[1].equals(data)){
                    sum=sum+Float.parseFloat( factura.getSumaDePlata().split(" ")[0]);
                    sum1=sum1+factura.getDiferentaCald()+factura.getDiferentaRece();
                }
            }
            sume_facturi.add(sum);
            mcApa.add(sum1);
        }



        for(int i=0;i<date_facturi.size(); i++){
             visitors.add(new BarEntry(Float.parseFloat(date_facturi.get(i)), sume_facturi.get(i )));
            visitors1.add(new BarEntry(Float.parseFloat(date_facturi.get(i)), mcApa.get(i )));
        }


        if(tipGraficSelectat.equals("Valori in lei")){ Log.v("pentrunoi", "log1");
            barDataSet=new BarDataSet(visitors, "Suma de plata (lei)");}


        spnTipGrafic.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                tipGraficSelectat=parent.getItemAtPosition(position).toString();
                Log.v("pentrunoi", tipGraficSelectat);


                if(tipGraficSelectat.equals("Valori in lei")) {
                    if (barChart1.getVisibility() == View.VISIBLE) {
                        barChart1.setVisibility(View.GONE);
                        barChart.setVisibility(View.VISIBLE);
                    }
                    Log.v("pentrunoi", "log1");
                    barDataSet = new BarDataSet(visitors, "Suma de plata (lei)");
                    barDataSet.setColors(ColorTemplate.rgb("5e17eb"), ColorTemplate.rgb("#ffbd59"), ColorTemplate.rgb("#5ce1e6"));
                    barDataSet.setValueTextColor(Color.BLACK);
                    barDataSet.setValueTextSize(16f);

                    BarData barData = new BarData(barDataSet);

                    barChart.setFitBars(true);
                    barChart.setData(barData);
                    barChart.getDescription().setText("Grafic cu bare");
                    barChart.animateY(2000);

                }

                else {
                    if(barChart.getVisibility() == View.VISIBLE) {barChart.setVisibility(View.GONE); barChart1.setVisibility(View.VISIBLE); }
                    Log.v("pentrunoi", "log2");
                    barDataSet=new BarDataSet(visitors1, "MC apa consumati");
                    barDataSet.setColors( ColorTemplate.rgb("5e17eb"), ColorTemplate.rgb("#ffbd59"),  ColorTemplate.rgb("#5ce1e6"));
                    barDataSet.setValueTextColor(Color.BLACK);
                    barDataSet.setValueTextSize(16f);

                    BarData barData1=new BarData(barDataSet);

                    barChart1.setFitBars(true);
                    barChart1.setData(barData1);
                    barChart1.getDescription().setText("Grafic cu bare");
                    barChart1.animateY( 2000);
                }


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });





    }






}