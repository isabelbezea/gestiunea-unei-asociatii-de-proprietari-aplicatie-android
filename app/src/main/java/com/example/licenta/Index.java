package com.example.licenta;

import java.io.Serializable;

public class Index implements Serializable {

    private String id;
    private float apaReceBaie;
    private float apaReceBucatarie;
    private float apaCaldaBaie;
    private float apaCaldaBucatarie;

    private String idUtilizator;
    private String lunaSiAnul;


    public Index() {
    }

    public Index(String id, float apaReceBaie, float apaReceBucatarie, float apaCaldaBaie, float apaCaldaBucatarie, String idUtilizator,String lunaSiAnul) {
        this.id = id;
        this.apaReceBaie = apaReceBaie;
        this.apaReceBucatarie = apaReceBucatarie;
        this.apaCaldaBaie = apaCaldaBaie;
        this.apaCaldaBucatarie = apaCaldaBucatarie;
        this.idUtilizator=idUtilizator;
        this.lunaSiAnul=lunaSiAnul;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public float getApaReceBaie() {
        return apaReceBaie;
    }

    public void setApaReceBaie(float apaReceBaie) {
        this.apaReceBaie = apaReceBaie;
    }

    public float getApaReceBucatarie() {
        return apaReceBucatarie;
    }

    public void setApaReceBucatarie(float apaReceBucatarie) {
        this.apaReceBucatarie = apaReceBucatarie;
    }

    public float getApaCaldaBaie() {
        return apaCaldaBaie;
    }

    public void setApaCaldaBaie(float apaCaldaBaie) {
        this.apaCaldaBaie = apaCaldaBaie;
    }

    public float getApaCaldaBucatarie() {
        return apaCaldaBucatarie;
    }

    public void setApaCaldaBucatarie(float apaCaldaBucatarie) {
        this.apaCaldaBucatarie = apaCaldaBucatarie;
    }

    public String getIdUtilizator() {
        return idUtilizator;
    }

    public void setIdUtilizator(String idUtilizator) {
        this.idUtilizator = idUtilizator;
    }

    public String getLunaSiAnul() {
        return lunaSiAnul;
    }

    public void setLunaSiAnul(String lunaSiAnul) {
        this.lunaSiAnul = lunaSiAnul;
    }



    @Override
    public String toString() {
        return "Index{" +
                "id='" + id + '\'' +
                ", apaReceBaie=" + apaReceBaie +
                ", apaReceBucatarie=" + apaReceBucatarie +
                ", apaCaldaBaie=" + apaCaldaBaie +
                ", apaCaldaBucatarie=" + apaCaldaBucatarie +
                ", idUtilizator='" + idUtilizator + '\'' +
                ", lunaSiAnul='" + lunaSiAnul + '\'' +

                '}';
    }
}
