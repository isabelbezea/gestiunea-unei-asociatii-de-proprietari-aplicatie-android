package com.example.licenta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;

import com.example.licenta.firebase.Callback;
import com.example.licenta.firebase.FirebaseServiceAvizier;
import com.example.licenta.firebase.FirebaseServiceContact;
import com.example.licenta.firebase.FirebaseServiceFactura;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AgendaActivity extends AppCompatActivity {

    FirebaseServiceContact firebaseServiceContact;

    private ListView lvContacte;
    private ImageView ivAdauga;
    public ContactAdapter contactAdapter;
    public static ArrayList<Contact> contacts_list = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agenda);


        lvContacte = findViewById(R.id.lv_contacte);
        ivAdauga=findViewById(R.id.iv_adauga_contact);

        addAdapter();

        firebaseServiceContact = FirebaseServiceContact.getInstance();
        firebaseServiceContact.attachDataChangeEventListener(dataChangeEventCallback());

        ivAdauga.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(AgendaActivity.this, AdaugaContactActivity.class);
                startActivity(intent);
            }
        });



    }


    private void notifyAdapter() {
        ContactAdapter adapter = (ContactAdapter) lvContacte.getAdapter();
        adapter.notifyDataSetChanged();
    }

    private void addAdapter() {

        contactAdapter = new ContactAdapter(contacts_list, this);
        lvContacte.setAdapter(contactAdapter);
    }


    private Callback<List<Contact>> dataChangeEventCallback() {
        return new Callback<List<Contact>>() {
            @Override
            public void runResultOnUiThread(List<Contact> result) {
                if (result != null) {
                    contacts_list.clear();
                    contacts_list.addAll(result);
                }

                Collections.reverse(contacts_list);
                notifyAdapter();
            }

        }
                ;

    }


}