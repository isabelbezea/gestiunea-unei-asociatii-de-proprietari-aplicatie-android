package com.example.licenta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.licenta.firebase.Callback;
import com.example.licenta.firebase.FirebaseServiceAvizier;
import com.example.licenta.firebase.FirebaseServiceContact;

import java.util.ArrayList;
import java.util.List;

public class AdaugaContactActivity extends AppCompatActivity {


    EditText etNume;
    EditText etFunctie;
    EditText etNrTelefon;
    Button btnTerminat;

    private  static ArrayList<Contact> contacts_list=new ArrayList<>() ;

    private FirebaseServiceContact firebaseService;
    Contact contact=null;

    String nume;
    String functie;
    String nrTelefon;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adauga_contact);

        etNume=findViewById(R.id.et_adauga_nume);
        etFunctie=findViewById(R.id.et_adauga_functie);
        etNrTelefon=findViewById(R.id.et_adauga_telefon);
        btnTerminat=findViewById(R.id.btn_terminat);

        firebaseService= FirebaseServiceContact.getInstance();

        Intent incomingIntent =getIntent();
        nume =incomingIntent.getStringExtra("numeContact");
        functie =incomingIntent.getStringExtra("functieContact");
        nrTelefon=incomingIntent.getStringExtra("nrTelefonContact");



        if(nrTelefon!=null)
        {
            etNume.setText(nume);
            etFunctie.setText(functie);
            etNrTelefon.setText(nrTelefon);


        }


        setupListeners();
        firebaseService.attachDataChangeEventListener(dataChangeEventCallback());



    }

    private boolean validateContent() {
        if (etNume.getText().toString().isEmpty()) {
            Toast.makeText(this, "Numele trebuie introdus!", Toast.LENGTH_LONG).show();
            return false;
        }
        if (etFunctie.getText().toString().isEmpty()) {
            Toast.makeText(this, "Functia trebuie introdusa!", Toast.LENGTH_LONG).show();
            return false;
        }

        if (etNrTelefon.getText().toString().isEmpty()) {
            Toast.makeText(this, "Numarul de telefon trebuie introdus!", Toast.LENGTH_LONG).show();
            return false;
        }


        return true;

    }

    private void setupListeners(){
        btnTerminat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateContent()) {
                    if(nrTelefon!=null){
                        Log.v("pentrunoi", "aici sunt");
                        for(Contact contact:contacts_list){
                            if(contact.getNumarTelefon().equals(nrTelefon)){

                                contact.setNumarTelefon(etNrTelefon.getText().toString());
                                contact.setNume(etNume.getText().toString());
                                contact.setFunctie(etFunctie.getText().toString());


                                firebaseService.upsert(contact);

                                Log.v("pentrunoi", "aici sunt");
                            }
                        }
                    }else {

                        Contact contact = new Contact();

                        contact.setNume(etNume.getText().toString());
                        contact.setFunctie(etFunctie.getText().toString());

                        contact.setNumarTelefon(etNrTelefon.getText().toString());


                        firebaseService.upsert(contact);}





                    Intent intent = new Intent(AdaugaContactActivity.this, AgendaActivity.class);
                    startActivity(intent);

                }
            }
        });
    }


    private Callback<List<Contact>> dataChangeEventCallback() {
        return new Callback<List<Contact>>() {
            @Override
            public void runResultOnUiThread(List<Contact> result) {
                if (result != null) {
                    contacts_list.clear();
                    contacts_list.addAll(result);

                }
            }

        };


    }

}