package com.example.licenta;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

import java.io.Serializable;


public class User implements Serializable {


    private String id;


    private String numeUtilizator;

    private String parola;

    private String email;

    private int nrApartament;

    private int nrPersoane;

    private String intrebareSecuritate;

    private String raspunsIntrebare;


    public User() {
    }

    public User(String id, String numeUtilizator, String parola, String email, int nrApartament, int nrPersoane, String intrebareSecuritate, String raspunsIntrebare) {
        this.id = id;
        this.numeUtilizator = numeUtilizator;
        this.parola = parola;
        this.email = email;
        this.nrApartament = nrApartament;
        this.nrPersoane = nrPersoane;
        this.intrebareSecuritate=intrebareSecuritate;
        this.raspunsIntrebare=raspunsIntrebare;
    }


    public User(String numeUtilizator, String parola, String email, int nrApartament, int nrPersoane, String intrebareSecuritate,String raspunsIntrebare) {
        this.numeUtilizator = numeUtilizator;
        this.parola = parola;
        this.email = email;
        this.nrApartament = nrApartament;
        this.nrPersoane = nrPersoane;
        this.intrebareSecuritate=intrebareSecuritate;
         this.raspunsIntrebare=raspunsIntrebare;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNumeUtilizator() {
        return numeUtilizator;
    }

    public void setNumeUtilizator(String numeUtilizator) {
        this.numeUtilizator = numeUtilizator;
    }

    public String getParola() {
        return parola;
    }

    public void setParola(String parola) {
        this.parola = parola;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getNrApartament() {
        return nrApartament;
    }

    public void setNrApartament(int nrApartament) {
        this.nrApartament = nrApartament;
    }

    public int getNrPersoane() {
        return nrPersoane;
    }

    public void setNrPersoane(int nrPersoane) {
        this.nrPersoane = nrPersoane;
    }

    public String getIntrebareSecuritate() {
        return intrebareSecuritate;
    }

    public void setIntrebareSecuritate(String intrebareSecuritate) {
        this.intrebareSecuritate = intrebareSecuritate;
    }

    public String getRaspunsIntrebare() {
        return raspunsIntrebare;
    }

    public void setRaspunsIntrebare(String raspunsIntrebare) {
        this.raspunsIntrebare = raspunsIntrebare;
    }

    @Override
    public String toString() {
        return "User{" +
                "id='" + id + '\'' +
                ", numeUtilizator='" + numeUtilizator + '\'' +
                ", parola='" + parola + '\'' +
                ", email='" + email + '\'' +
                ", nrApartament=" + nrApartament +
                ", nrPersoane=" + nrPersoane +
                ", intrebareSecuritate='" + intrebareSecuritate + '\'' +
                ", raspunsIntrebare='" + raspunsIntrebare + '\'' +
                '}';
    }
}
