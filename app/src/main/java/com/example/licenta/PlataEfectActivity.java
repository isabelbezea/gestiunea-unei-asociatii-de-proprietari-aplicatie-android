package com.example.licenta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class PlataEfectActivity extends AppCompatActivity {

    Button btnInapoi;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_plata_efect);

        btnInapoi=findViewById(R.id.btn_inapoi);
        btnInapoi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent= new Intent(PlataEfectActivity.this,  Dashboard.class);
                startActivity(intent);
            }
        });
    }
}