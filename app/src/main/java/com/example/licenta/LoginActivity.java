package com.example.licenta;

import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.os.Bundle;
import android.util.Log;

import com.google.android.material.tabs.TabLayout;



public class LoginActivity extends AppCompatActivity {

    TabLayout tabLayout;
    ViewPager viewPager;
    float v=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        tabLayout=findViewById(R.id.tab_layout);
        viewPager=findViewById(R.id.view_pager);


        tabLayout.addTab(tabLayout.newTab().setText("Intra in cont"));
        tabLayout.addTab(tabLayout.newTab().setText("Creeaza cont"));

        tabLayout.setTabGravity(TabLayout.GRAVITY_FILL);
     //   tabLayout.setupWithViewPager(viewPager);

        final LoginAdapter adapter=new LoginAdapter(getSupportFragmentManager(), this, tabLayout.getTabCount());
        viewPager.setAdapter(adapter);
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));

        tabLayout.setTranslationY(300);
        tabLayout.setAlpha(v);
       tabLayout.animate().translationY(0).alpha(1).setDuration(1000).setStartDelay(400).start();


    }
}