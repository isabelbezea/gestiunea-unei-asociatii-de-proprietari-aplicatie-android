package com.example.licenta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.LinearLayout;

import com.example.licenta.firebase.Callback;
import com.example.licenta.firebase.FirebaseService;
import com.example.licenta.firebase.FirebaseServiceFactura;

import java.util.ArrayList;
import java.util.List;

public class DashboardActivity extends AppCompatActivity {


    LinearLayout llIntroducereCheltuieliGenerale;
    LinearLayout llAgenda;
    LinearLayout llDocumente;
    LinearLayout llGestiune;
    LinearLayout llAvizier;
    LinearLayout llStatistici;

    private FirebaseService firebaseServiceUser;
    public   static ArrayList<User> users_list=new ArrayList<>() ;

    private FirebaseServiceFactura firebaseServiceFactura;
    public   static ArrayList<Factura> facturi_list=new ArrayList<>() ;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard2);

        llIntroducereCheltuieliGenerale=findViewById(R.id.ll_introducereA) ;

        llAgenda=findViewById(R.id.ll_agendaA);

        llDocumente=findViewById(R.id.ll_documenteA);

        llGestiune=findViewById(R.id.ll_gestiuneA);

        llAvizier=findViewById(R.id.ll_avizierA) ;

        llStatistici=findViewById(R.id.ll_statisticiA) ;

        firebaseServiceFactura= FirebaseServiceFactura.getInstance();
        firebaseServiceFactura.attachDataChangeEventListener(dataChangeEventCallback());

        firebaseServiceUser= FirebaseService.getInstance();
        firebaseServiceUser.attachDataChangeEventListener(dataChangeEventCallback2());


        llIntroducereCheltuieliGenerale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(DashboardActivity.this, IntroducereCheltuieliBlocActivity.class);
                startActivity(intent);

            }
        });

        llAgenda.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(DashboardActivity.this, AgendaActivity.class);
                startActivity(intent);

            }
        });

        llDocumente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(DashboardActivity.this, DocumentActivity.class);
                startActivity(intent);

            }
        });


        llGestiune.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(DashboardActivity.this,    ManagingActivity.class);
                startActivity(intent);

            }
        });


        llAvizier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(DashboardActivity.this,    AvizierActivity.class);
                startActivity(intent);

            }
        });



        llStatistici.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(DashboardActivity.this,    StatisticiAdministratorActivity.class);
                startActivity(intent);

            }
        });





    }

    private Callback<List<Factura>> dataChangeEventCallback() {
        return new Callback<List<Factura>>() {
            @Override
            public void runResultOnUiThread(List<Factura> result) {
                if (result != null) {
                    facturi_list.clear();
                    facturi_list.addAll(result);

                }
            }

        };


    }

    private Callback<List<User>> dataChangeEventCallback2() {
        return new Callback<List<User>>() {
            @Override
            public void runResultOnUiThread(List<User> result) {
                if (result != null) {
                    users_list.clear();
                    users_list.addAll(result);

                }
            }

        };


    }

}