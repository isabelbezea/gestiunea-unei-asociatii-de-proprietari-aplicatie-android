package com.example.licenta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;

import com.example.licenta.firebase.Callback;
import com.example.licenta.firebase.FirebaseServiceAvizier;
import com.example.licenta.firebase.FirebaseServiceFactura;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class AvizierActivity extends AppCompatActivity {

    Button btnContact;
    ImageView ivAdauga;
    FirebaseServiceAvizier firebaseService;
    private ListView lvAvizier;
    public AvizierAdapter avizierAdapter;
    public static ArrayList<Avizier> news_list = new ArrayList<>();
    Avizier avizier = null;
    int selectedPosition = -1;

     public static  int countClick=0;
    public static  int countClick1=0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_avizier);


        lvAvizier = findViewById(R.id.lv_avizier);

        addAdapter();
        Log.v("pentrunoi", "CE");

        firebaseService = FirebaseServiceAvizier.getInstance();
        firebaseService.attachDataChangeEventListener(dataChangeEventCallback());


        btnContact = findViewById(R.id.btn_contacteaza_administrator);
        ivAdauga = findViewById(R.id.iv_adauga);

//        coachSelectEventListener();
//        deleteEventListener();

        Log.v("pentrunoi", LoginTabFragment.OKadministrator.toString()+"!!!!!!!!!!");

        if (LoginTabFragment.OKadministrator) {

            ivAdauga.setVisibility(View.VISIBLE);
            Log.v("pentrunoi", LoginTabFragment.OKadministrator.toString()+"!!!!!!!!!!");
            ivAdauga.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(AvizierActivity.this, AdaugaAvizierActivity.class);
                    startActivity(intent);
                }
            });
        } else {
            Log.v("pentrunoi", "am intrat");
            btnContact.setVisibility(View.VISIBLE);
            btnContact.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(Intent.ACTION_SENDTO);
                    intent.setData(Uri.parse("mailto:")); // only email apps should handle this
                    intent.putExtra(Intent.EXTRA_EMAIL, new String[]{"administratorASLT@gmail.com"});
                  ///  intent.putExtra(Intent.EXTRA_SUBJECT, "lalala");
                    if (intent.resolveActivity(getPackageManager()) != null) {
                        startActivity(intent);
                    }
                }
            });
        }

        if (AvizierAdapter.okL) {
            {
                for (Avizier avizier : news_list) {
                    if (avizier.getTitlu().equals(AvizierAdapter.titlu)) {
                       if(countClick==0){
                        avizier.setLikes(avizier.getLikes() + 1);
                        countClick=1;
                       Log.v("pentrunoi", countClick+"if1");
                         }

                       else {
                           avizier.setLikes(avizier.getLikes() - 1);
                           countClick=0;
                           Log.v("pentrunoi", "if2");
                       }
                       }
                    Log.v("pentrunoi", countClick+"dupa ce a iesit");
                        firebaseService.upsert(avizier);
                        Log.v("pentrunoi", "da: " + AvizierAdapter.nrLikes + 1);
                        AvizierAdapter.okL = false;
                    }
                }
            }



        if (AvizierAdapter.okD) {
            {
                for (Avizier avizier : news_list) {
                    if (avizier.getTitlu().equals(AvizierAdapter.titlu)) {
                        if(countClick1==0){
                        avizier.setDislikes(avizier.getDislikes() + 1);
                        countClick1=1;}
                        else {
                            avizier.setDislikes(avizier.getDislikes()- 1);
                            countClick1=0;
                        }
                        firebaseService.upsert(avizier);
                        AvizierAdapter.okD = false;

                    }
                }
            }
        }

    }

    private void notifyAdapter() {
        AvizierAdapter adapter = (AvizierAdapter) lvAvizier.getAdapter();
        adapter.notifyDataSetChanged();
    }

    private void addAdapter() {
        Log.v("pentrunoi", "count" + news_list.size());

        avizierAdapter = new AvizierAdapter(news_list, this);
        lvAvizier.setAdapter(avizierAdapter);
    }

    private Callback<List<Avizier>> dataChangeEventCallback() {
        return new Callback<List<Avizier>>() {
            @Override
            public void runResultOnUiThread(List<Avizier> result) {
                if (result != null) {
                    news_list.clear();
                    news_list.addAll(result);
                }

                Collections.reverse(news_list);
                notifyAdapter();
            }

        }
                ;

    }


}
