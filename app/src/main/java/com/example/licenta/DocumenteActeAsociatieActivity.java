package com.example.licenta;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.service.autofill.Dataset;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;

import java.util.ArrayList;
import java.util.List;

public class DocumenteActeAsociatieActivity extends AppCompatActivity {


    ListView lvActeAsociatie;
    DatabaseReference databaseReference;
    List<PDF>  listaPdf1 ;
    DocumentAdapter documentAdapter;

    ArrayList<PDF> uploads1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_documente_acte_asociatie);

        lvActeAsociatie=findViewById(R.id.lv_acte_asociatie);

        listaPdf1=new ArrayList<>();

        viewAllFiles();

        lvActeAsociatie.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                PDF pdf= listaPdf1.get(position);

                Intent intent=new Intent();
                intent.setData(Uri.parse(pdf.getUrl()));
                startActivity(intent);
            }
        });




        lvActeAsociatie.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int position, long id) {

                PDF pdf= uploads1.get(position);

                Log.v("pentrunoi", pdf.getUrl()+"aici fa");
                StorageReference reference= FirebaseStorage.getInstance().getReferenceFromUrl(pdf.getUrl());


                reference.delete().addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {


                        Toast.makeText(DocumenteActeAsociatieActivity.this, "Stergerea a fost efectuata cu succes", Toast.LENGTH_LONG);

                    }
                }).addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {

                        Toast.makeText(DocumenteActeAsociatieActivity.this, "Stergerea nu a fost efectuata cu succes", Toast.LENGTH_LONG);

                    }
                });


                return true;

            }
        });

    }

    private void viewAllFiles() {
        databaseReference= FirebaseDatabase.getInstance().getReference("documents/");
        Log.v("pentrunoi", "!!!!!!!!"+databaseReference.toString());
        databaseReference.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                for(DataSnapshot postSnapshot: snapshot.getChildren()){
                    PDF pdf=postSnapshot.getValue(PDF.class);
                    listaPdf1.add(pdf);


                }

               uploads1= new ArrayList<>();
//                String[] uploadsDenumire=new String[listaPdf.size()];
//                String[] uploadsDescriere=new String[listaPdf.size()];



                for(int i=0;i<listaPdf1.size();i++){
                    PDF obiect=new PDF();
                    obiect.setDenumire(listaPdf1.get(i).getDenumire());
                    obiect.setDescriere(listaPdf1.get(i).getDescriere());
                    obiect.setCategorie(listaPdf1.get(i).getCategorie());

                    obiect.setUrl(listaPdf1.get(i).getUrl());


                    if(obiect.getCategorie().equals("ACTE ASOCIATIE"))
                    {
                        uploads1.add(obiect);
                    }

                       }

//                for(int i=0;i<uploadsDenumire.length;i++){
//                    uploadsDenumire[i]=listaPdf.get(i).getDenumire();
//                }
//
//                for(int i=0;i<uploadsDescriere.length;i++){
//                    uploadsDescriere[i]=listaPdf.get(i).getDescriere();
//                }


                addAdapter();



            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {

            }
        });
    }

    private void notifyAdapter() {
        ContactAdapter adapter = (ContactAdapter) lvActeAsociatie.getAdapter();
        adapter.notifyDataSetChanged();
    }

    private void addAdapter() {

        documentAdapter = new DocumentAdapter(uploads1, this);
        lvActeAsociatie.setAdapter(documentAdapter);
    }



}