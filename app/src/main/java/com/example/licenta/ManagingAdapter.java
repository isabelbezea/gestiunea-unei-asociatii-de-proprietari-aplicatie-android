package com.example.licenta;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;

public class ManagingAdapter  extends BaseAdapter {
    private ArrayList<InformatiiSuplimentare> informations;
    private Context context;
    private LayoutInflater inflater;

    public static String numePrenumeT;

    public ManagingAdapter( ArrayList<InformatiiSuplimentare> informations, Context context) {
        this.informations = informations;
        this.context = context;
        this.inflater = LayoutInflater.from(context);

    }

    @Override
    public int getCount() {
        return informations.size();
    }

    @Override
    public Object getItem(int position) {
        return informations.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        final View locatarView=inflater.inflate(R.layout.locatar_item, viewGroup, false);
        final TextView tvNumePrenume =locatarView.findViewById(R.id.tv_nume_prenume);
        final TextView tvTipLocatar=locatarView.findViewById(R.id.tv_tip_locatar);



        InformatiiSuplimentare informatiiSuplimentare=informations.get(position);

        String numePrenume= informatiiSuplimentare.getNume()+" " + informatiiSuplimentare.getPrenume();

        tvNumePrenume.setText(numePrenume);
        tvTipLocatar.setText(informatiiSuplimentare.getTipLocatar());


        locatarView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                numePrenumeT=numePrenume;
                Intent intent=new Intent(context, VizualizareDetaliiActivity.class);
                context.startActivity( intent);
            }
        });

        return locatarView;

    }
}
