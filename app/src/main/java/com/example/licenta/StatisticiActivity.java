package com.example.licenta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import com.example.licenta.firebase.Callback;
import com.example.licenta.firebase.FirebaseServiceFactura;
import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.charts.RadarChart;
import com.github.mikephil.charting.components.XAxis;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.data.PieEntry;
import com.github.mikephil.charting.data.RadarData;
import com.github.mikephil.charting.data.RadarDataSet;
import com.github.mikephil.charting.data.RadarEntry;
import com.github.mikephil.charting.formatter.IndexAxisValueFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.List;

import cn.iwgang.countdownview.CountdownView;

public class StatisticiActivity extends AppCompatActivity {

    public FirebaseServiceFactura firebaseServiceFactura;

    public static ArrayList<Factura> facturi_list = new ArrayList<>();

    ArrayList<BarEntry> visitors=new ArrayList<>();

    public BarChart barChart;
    public PieChart  pieChart ;
    public RadarChart radarChart;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_statistici);

        firebaseServiceFactura = FirebaseServiceFactura.getInstance();
        firebaseServiceFactura.attachDataChangeEventListener(dataChangeEventCallback());

        Calendar cal = Calendar.getInstance();

        int month = cal.get(Calendar.MONTH)+1;
        int year = cal.get(Calendar.YEAR);


        Button btnBare=findViewById(R.id.btn_grafic_bare);

        barChart= findViewById(R.id.grafic_bare);

        pieChart=findViewById(R.id.grafic_proportii);

        radarChart=findViewById(R.id.grafic_radar);



        btnBare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

              if(pieChart.getVisibility()==View.VISIBLE)
              {  pieChart.setVisibility(View.GONE);}
              else if(radarChart.getVisibility()==View.VISIBLE) {
                  radarChart.setVisibility(View.GONE);

              }




                barChart.setVisibility(View.VISIBLE);

                for(Factura factura : facturi_list){

                    Log.v("pentrunoi", "!!!!!!"+factura.toString()+"!!");
                    visitors.add(new BarEntry(Float.parseFloat(factura.getDataScadenta().split("/")[1]),Float.parseFloat( factura.getSumaDePlata().split(" ")[0])));
                }



                Log.v("pentrunoi", facturi_list.size()+".");

                BarDataSet barDataSet=new BarDataSet(visitors, "Suma de plata (lei)");

                barDataSet.setColors( ColorTemplate.rgb("5e17eb"), ColorTemplate.rgb("#ffbd59"),  ColorTemplate.rgb("#5ce1e6"));
                barDataSet.setValueTextColor(Color.BLACK);
                barDataSet.setValueTextSize(16f);

                BarData barData=new BarData(barDataSet);

                barChart.setFitBars(true);
                barChart.setData(barData);
                barChart.getDescription().setText("Grafic cu bare");
                barChart.animateY( 2000);





            }
        });

        Button btnProportii=findViewById(R.id.btn_grafic_pie);
        btnProportii.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(barChart.getVisibility()==View.VISIBLE)
                { barChart.setVisibility(View.GONE); }
                else if(radarChart.getVisibility()==View.VISIBLE) {
                    radarChart.setVisibility(View.GONE);

                }

                pieChart.setVisibility(View.VISIBLE);

                ArrayList<PieEntry> visitors=new ArrayList<>();
                for(Factura factura : facturi_list){

                    Log.v("pentrunoi", "!!!!!!"+factura.toString()+"!!");
                    visitors.add(new PieEntry(Float.parseFloat( factura.getSumaDePlata().split(" ")[0]), factura.getDataScadenta()));
                }

                PieDataSet pieDataSet=new PieDataSet(visitors, "Suma de plata (lei)");
                pieDataSet.setColors( ColorTemplate.rgb("5e17eb"), ColorTemplate.rgb("#ffbd59"), ColorTemplate.rgb("#5ce1e6"));
                pieDataSet.setValueTextColor(Color.BLACK);
                pieDataSet.setValueTextSize( 16f);

                PieData pieData=new PieData(pieDataSet);
                pieChart.setData(pieData);
                pieChart.getDescription().setEnabled(false);
                pieChart.setCenterText("Visitors");
                pieChart.animate();


            }
        });

        Button btnRadar=findViewById(R.id.btn_grafic_radar) ;
        btnRadar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(barChart.getVisibility()==View.VISIBLE)
                { barChart.setVisibility(View.GONE); }
                else if(pieChart.getVisibility()==View.VISIBLE){
                    pieChart.setVisibility(View.GONE);
                }

                radarChart.setVisibility(View.VISIBLE);


                ArrayList<RadarEntry> visitors=new ArrayList<>();
                for(Factura factura : facturi_list){

                    Log.v("pentrunoi", "!!!!!!"+factura.toString()+"!!");
                    visitors.add(new RadarEntry(Float.parseFloat( factura.getSumaDePlata().split(" ")[0])));
                }

                for(RadarEntry r:visitors){
                    Log.v("pentrunoi", r.toString()+"hihi");
                }


                RadarDataSet radarDataSet=new RadarDataSet(visitors, "Suma de plata (lei)");
                radarDataSet.setColor( ColorTemplate.rgb("5e17eb"));
                radarDataSet.setLineWidth(2f);
                radarDataSet.setValueTextColor(Color.BLACK);
                radarDataSet.setValueTextSize( 16f);

                RadarData radarData=new RadarData();
                radarData.addDataSet(radarDataSet);

                ArrayList<String> labels=new ArrayList<>();
                for(Factura factura:facturi_list){
                    labels.add(factura.getDataScadenta()) ;
                }
                Log.v("pentrunoi", labels.size()+":)");

                XAxis xAxis=radarChart.getXAxis();
                xAxis.setValueFormatter(new IndexAxisValueFormatter(labels));

                radarChart.setData(radarData);




            }
        });

        CountdownView mCvCountdownView = (CountdownView)findViewById(R.id.cv_countdownViewTest1);
        SimpleDateFormat sdf= new SimpleDateFormat( "dd-MM-yyyy");

        String countDate;

        if(month<=9)
        {
            countDate="28-0"+month+"-"+year;}
        else{
            countDate="28-"+month+"-"+year;
        }

        Log.v("pentrunoi", countDate);
        Date now=new Date();

        try{
            Date date=sdf.parse(countDate);
            long currentTime=now.getTime();
            long countdown=date.getTime();

            long countdownto=countdown-currentTime;

            mCvCountdownView.start(countdownto);

        }
        catch(ParseException e){
            e.printStackTrace();
        }




    }

    private Callback<List<Factura>> dataChangeEventCallback() {
        return new Callback<List<Factura>>() {
            @Override
            public void runResultOnUiThread(List<Factura> result) {
                if (result != null) {
                    facturi_list.clear();
                    Log.v("pentrunoi", result.toString());

                    for (Factura factura : result) {

                        if (factura.getIdUtilizator().equals(LoginTabFragment.ID)) {

                            Log.v("pentrunoi", "am intrat");
                            facturi_list.add(factura);
                            Log.v("pentrunoi", factura.getDataScadenta().split("/")[1]);

                        }



                    }
                    Log.v("pentrunoi", facturi_list.size()+"!");




                }


            }
        };
    }



}