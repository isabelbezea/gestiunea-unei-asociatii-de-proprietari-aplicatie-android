package com.example.licenta;

import java.io.Serializable;

public class Contact  implements Serializable {

    private String id;
    private String nume;
    private String functie;
    private String numarTelefon;

    public Contact() {
    }

    public Contact(String id, String nume, String functie, String numarTelefon) {
        this.id = id;
        this.nume = nume;
        this.functie = functie;
        this.numarTelefon = numarTelefon;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getFunctie() {
        return functie;
    }

    public void setFunctie(String functie) {
        this.functie = functie;
    }

    public String getNumarTelefon() {
        return numarTelefon;
    }

    public void setNumarTelefon(String numarTelefon) {
        this.numarTelefon = numarTelefon;
    }

    @Override
    public String toString() {
        return "Contact{" +
                "id='" + id + '\'' +
                ", nume='" + nume + '\'' +
                ", functie='" + functie + '\'' +
                ", numarTelefon='" + numarTelefon + '\'' +
                '}';
    }
}
