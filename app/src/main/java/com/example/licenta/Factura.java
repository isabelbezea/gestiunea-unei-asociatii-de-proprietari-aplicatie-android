package com.example.licenta;

import java.io.Serializable;

public class Factura implements Serializable {

    private String  id;
    private String dataScadenta;
    private  String sumaDePlata;
    private  boolean platita;
    private String idUtilizator;
    private float diferentaCald;
    private float diferentaRece;
    private float sumaRestanta;

    private String idCheltuieliGenerale;



    public Factura() {
    }

    public Factura(String id, String dataScadenta, String sumaDePlata, boolean platita, String idUtilizator, float diferentaCald, float diferentaRece, float sumaRestanta) {
        this.id = id;
        this.dataScadenta = dataScadenta;
        this.sumaDePlata = sumaDePlata;
        this.platita = platita;
        this.idUtilizator = idUtilizator;
     this.diferentaCald=diferentaCald;
     this.diferentaRece=diferentaRece ;
     this.sumaRestanta=sumaRestanta;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDataScadenta() {
        return dataScadenta;
    }

    public void setDataScadenta(String dataScadenta) {
        this.dataScadenta = dataScadenta;
    }

    public String getSumaDePlata() {
        return sumaDePlata;
    }

    public void setSumaDePlata(String sumaDePlata) {
        this.sumaDePlata = sumaDePlata;
    }

    public boolean isPlatita() {
        return platita;
    }

    public void setPlatita(boolean platita) {
        this.platita = platita;
    }

    public String getIdUtilizator() {
        return idUtilizator;
    }

    public void setIdUtilizator(String idUtilizator) {
        this.idUtilizator = idUtilizator;
    }

    public float getDiferentaCald() {
        return diferentaCald;
    }

    public void setDiferentaCald(float diferentaCald) {
        this.diferentaCald = diferentaCald;
    }

    public float getDiferentaRece() {
        return diferentaRece;
    }

    public void setDiferentaRece(float diferentaRece) {
        this.diferentaRece = diferentaRece;
    }

    public float getSumaRestanta() {
        return sumaRestanta;
    }

    public void setSumaRestanta(float sumaRestanta) {
        this.sumaRestanta = sumaRestanta;
    }


    public String getIdCheltuieliGenerale() {
        return idCheltuieliGenerale;
    }

    public void setIdCheltuieliGenerale(String idCheltuieliGenerale) {
        this.idCheltuieliGenerale = idCheltuieliGenerale;
    }

    @Override
    public String toString() {
        return "Factura{" +
                "id='" + id + '\'' +
                ", dataScadenta='" + dataScadenta + '\'' +
                ", sumaDePlata='" + sumaDePlata + '\'' +
                ", platita=" + platita +
                ", idUtilizator='" + idUtilizator + '\'' +
                ", diferentaCald=" + diferentaCald +
                ", diferentaRece=" + diferentaRece +
                '}';
    }
}
