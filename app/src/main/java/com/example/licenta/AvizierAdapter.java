package com.example.licenta;

import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.example.licenta.firebase.FirebaseServiceAvizier;
import com.example.licenta.firebase.FirebaseServiceFactura;

import java.util.ArrayList;

public class AvizierAdapter extends BaseAdapter {

    private ArrayList<Avizier> news;
    private Context context;
    private LayoutInflater inflater;

    FirebaseServiceAvizier firebaseService;
    public static ArrayList<Avizier> news_list = new ArrayList<>();

    public static int nrLikes=0;
    public static int nrDislikes=0;
    public static Boolean okL=false;
    public static Boolean okD=false;
 int selectedPosition=-1;
    public static String titlu;



    public AvizierAdapter(ArrayList<Avizier> news, Context context) {
        this.news=  news;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }


    @Override
    public int getCount() {
        return news.size();
    }

    @Override
    public Object getItem(int position) {
        return  news.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        final View avizierView=inflater.inflate(R.layout.avizier_item, viewGroup, false);
        final TextView tvTitlu =avizierView.findViewById(R.id.tv_titllu);
        final TextView tvDescriere=avizierView.findViewById(R.id.tv_descriere);
        final TextView tvLikes=avizierView.findViewById(R.id.tv_likes);
        final TextView tvDisLikes=avizierView.findViewById(R.id.tv_dislikes);

        ImageView ivEdit=avizierView.findViewById(R.id.iv_edit);
       // ImageView ivDelete=avizierView.findViewById(R.id.iv_delete);




        final ImageView ivLike=avizierView.findViewById(R.id.iv_like);
        ImageView ivDislike=avizierView.findViewById(R.id.iv_dislike);


        if(LoginTabFragment.OKadministrator){
            ivEdit.setVisibility(View.VISIBLE);
          //ivDelete.setVisibility(View.VISIBLE);
        }





        final Avizier avizier=news.get(position);
        selectedPosition=position;
        Log.v("pentrunoi", "pozitia"+selectedPosition);

        ivEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                 Intent intent=new Intent( context, AdaugaAvizierActivity.class);
                intent.putExtra("titluOBIECT", avizier.getTitlu());
                Log.v("pentrunoi", "TITLU"+avizier.getTitlu());
                intent.putExtra("descriereOBIECT", avizier.getDenumire());

                context.startActivity(intent);
            }
        });

//        ivDelete.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                {
//
//                }}}) ;




       //Log.v("pentrunoi", "ID: "+IDanunt);

        tvTitlu.setText(avizier.getTitlu());
        tvDescriere.setText(avizier.getDenumire());


        ivLike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
           //     nrLikes= Integer.parseInt(tvLikes.getText().toString());
                titlu=tvTitlu.getText().toString();
                okL=true;

                Intent intent=new Intent(context, AvizierActivity.class);
                context.startActivity( intent);
            }
        });

        ivDislike.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
              //    nrDislikes= Integer.parseInt(tvDisLikes.getText().toString());


                titlu=tvTitlu.getText().toString();
                okD=true;
                Intent intent=new Intent(context, AvizierActivity.class);
                context.startActivity( intent);
            }
        });


        tvLikes.setText(String.valueOf(avizier.getLikes()));
        tvDisLikes.setText(String.valueOf(avizier.getDislikes()));



        return avizierView;








    }
}
