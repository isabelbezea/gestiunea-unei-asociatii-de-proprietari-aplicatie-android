package com.example.licenta;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;

public class FacturiInexistenteActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facturi_inexistente);
    }
}