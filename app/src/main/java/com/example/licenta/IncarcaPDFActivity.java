package com.example.licenta;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ProgressBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class IncarcaPDFActivity extends AppCompatActivity {

    Button btnFinalizeaza;
    Spinner spnCatgeorie;
    EditText etDenumire;
    EditText etDescriere;
    TextView tvFisier;

    StorageReference storageReference;
    DatabaseReference databaseReference;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_incarca_p_d_f);

      //  btnFinalizeaza=findViewById(R.id.btn_finalizeaza);
        spnCatgeorie=findViewById(R.id.spn_categorie);
        etDenumire=findViewById(R.id.et_denumire);
        etDescriere=findViewById(R.id.et_descriere);
        tvFisier=findViewById(R.id.tv_rasfoieste);

        storageReference= FirebaseStorage.getInstance().getReference();
        databaseReference= FirebaseDatabase.getInstance().getReference("documents");



        tvFisier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectPDFFile();
            }
        });






    }

    private void selectPDFFile(){
        Intent intent=new Intent();
        intent.setType("application/pdf");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        startActivityForResult(Intent.createChooser(intent, "Selecteaza fisierul PDF"),1);


    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode==1 && resultCode==RESULT_OK && data!=null && data.getData()!=null){
            uploadPDFFile(data.getData());
        }
    }

    private void uploadPDFFile(Uri data){


        final ProgressDialog progressDialog=new ProgressDialog(this);

        progressDialog.setTitle("Se incarca...");
        progressDialog.show();

        Log.v("pentrunoi",spnCatgeorie.getSelectedItem().toString() );
            StorageReference reference=storageReference.child("documents/"+spnCatgeorie.getSelectedItem()+"/"+etDenumire.getText()+".pdf");
            Log.v("pentrunoi", reference.toString());
            reference.putFile(data).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                    Log.v("pentrunoi", "am intrat1");
                    Task<Uri> uri=taskSnapshot.getStorage().getDownloadUrl();
                    Log.v("pentrunoi", uri.toString()+"!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
                    while(!uri.isComplete());
                    Uri url =  uri.getResult();


                    Log.v("pentrunoi", "am intrat2");

                    PDF pdf=new PDF(spnCatgeorie.getSelectedItem().toString(), etDenumire.getText().toString(), etDescriere.getText().toString(), url.toString());

                    Log.v("pentrunoi", "am intrat3");

                    databaseReference.child(databaseReference.push().getKey()).setValue(pdf);

                    Toast.makeText(IncarcaPDFActivity.this, "Fisier incarcat!", Toast.LENGTH_SHORT).show();
                    progressDialog.dismiss();



                }
            }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
                @Override
                public void onProgress(@NonNull UploadTask.TaskSnapshot snapshot) {


                    double progress=(100.0*snapshot.getBytesTransferred())/snapshot.getTotalByteCount();
                    Log.v("pentrunoi", snapshot.getTotalByteCount()+"  siiiii "+snapshot.getBytesTransferred());

                    progressDialog.setMessage("Incarcat: "+(int)progress+"%");


                }
            }).addOnFailureListener(new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    Log.v("pentrunoi", "am crapat");
                }
            });



    }


}