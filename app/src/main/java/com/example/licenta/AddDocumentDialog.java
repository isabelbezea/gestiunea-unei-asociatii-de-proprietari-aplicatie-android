package com.example.licenta;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatDialogFragment;

import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.OnProgressListener;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

public class AddDocumentDialog extends AppCompatDialogFragment {

    Spinner spnCategorie;
    EditText etTitlu;
    EditText  etDescriere;
    TextView tvRasfoieste;

    String categorieSelectata;
    StorageReference storageReference;
    DatabaseReference databaseReference;

    @NonNull
    @Override
    public Dialog onCreateDialog(@Nullable Bundle savedInstanceState) {
        final AlertDialog.Builder  builder=new AlertDialog.Builder(getActivity(), R.style.AlertDialogCustom);

        LayoutInflater inflater=getActivity().getLayoutInflater();
        View view = inflater.inflate(R.layout.dialog_warn, null);

        builder.setView(view).setTitle("Incarca document").setNegativeButton("Anuleaza", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        }).setPositiveButton("Finalizeaza", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {

            }
        });



        spnCategorie=view.findViewById(R.id.spn_categorie);
        etTitlu=view.findViewById(R.id.et_denumire);
        etDescriere =view.findViewById(R.id.et_descriere);
        tvRasfoieste=view.findViewById(R.id.tv_rasfoieste);



        storageReference= FirebaseStorage.getInstance().getReference();
        databaseReference= FirebaseDatabase.getInstance().getReference("uploads");

        tvRasfoieste.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectPDFFile();

            }
        });
        spnCategorie.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                categorieSelectata=parent.getItemAtPosition(position).toString();

                           }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        return  builder.create() ;
    }

    private void   selectPDFFile(){
        Intent intent = new Intent();
        intent.setType("application/pdf");
        intent.setAction(Intent.ACTION_GET_CONTENT);
        Log.v("pentrunoi", "hehe1");
        startActivityForResult(Intent.createChooser(intent, "Selectati fisierul PDF"), 1);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Log.v("pentrunoi", "hehe");
        if(requestCode==1 && data!=null && data.getData()!= null){
            Log.v("pentrunoi", data.getData().getPath());

            uploadPDFFile(data.getData());


        }
    }

    private void uploadPDFFile(final Uri data){
        Log.v("pentrunoi", "hehe2");
        StorageReference reference=storageReference.child("uploads/"+System.currentTimeMillis()+".pdf");

        Log.v("pentrunoi", data.getPath()+" ss");
        reference.putFile(data).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {

                Log.v("pentrunoi", "hehe5");
                Task<Uri> uri = taskSnapshot.getStorage().getDownloadUrl() ;

                while(!uri.isComplete());
                Uri url=uri.getResult();

                    PDF pdf= new PDF(categorieSelectata, etTitlu.getText().toString(),etDescriere.getText().toString(),url.toString());
                    databaseReference.child(databaseReference.push().getKey()).setValue(pdf);
                    Log.v("pentrunoi", "am ajuns aici!") ;



            }
        }).addOnProgressListener(new OnProgressListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onProgress(@NonNull UploadTask.TaskSnapshot snapshot) {
                Log.v("pentrunoi", "hehe6");
                dismiss();
            }
        });
    }

}
