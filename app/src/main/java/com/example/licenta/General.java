package com.example.licenta;

import java.io.Serializable;

public class General implements Serializable {

    private String id;
    private float gazeNaturale;
    private float energieElectrica;
    private float deseuriMenajere;
    private float calduraPartiComune;
    private float apaPluviala;
    private float serviciiCuratenie;
    private float serviciiContabilitate;
    private float mandat;
    private String data;
    private float caldura;
    private float apaRece;
    private float apaCalda;
    public General() {
    }

    public General(String id, float gazeNaturale, float energieElectrica, float deseuriMenajere, float calduraPartiComune, float apaPluviala, float serviciiCuratenie, float serviciiContabilitate, float mandat, String data, float caldura) {
        this.id=id;
        this.gazeNaturale = gazeNaturale;
        this.energieElectrica = energieElectrica;
        this.deseuriMenajere = deseuriMenajere;
        this.calduraPartiComune = calduraPartiComune;
        this.apaPluviala = apaPluviala;
        this.serviciiCuratenie = serviciiCuratenie;
        this.serviciiContabilitate = serviciiContabilitate;
        this.mandat = mandat;
        this.data=data;
        this.caldura=caldura;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public float getGazeNaturale() {
        return gazeNaturale;
    }

    public void setGazeNaturale(float gazeNaturale) {
        this.gazeNaturale = gazeNaturale;
    }

    public float getEnergieElectrica() {
        return energieElectrica;
    }

    public void setEnergieElectrica(float energieElectrica) {
        this.energieElectrica = energieElectrica;
    }

    public float getDeseuriMenajere() {
        return deseuriMenajere;
    }

    public void setDeseuriMenajere(float deseuriMenajere) {
        this.deseuriMenajere = deseuriMenajere;
    }

    public float getCalduraPartiComune() {
        return calduraPartiComune;
    }

    public void setCalduraPartiComune(float calduraPartiComune) {
        this.calduraPartiComune = calduraPartiComune;
    }

    public float getApaPluviala() {
        return apaPluviala;
    }

    public void setApaPluviala(float apaPluviala) {
        this.apaPluviala = apaPluviala;
    }

    public float getServiciiCuratenie() {
        return serviciiCuratenie;
    }

    public void setServiciiCuratenie(float serviciiCuratenie) {
        this.serviciiCuratenie = serviciiCuratenie;
    }

    public float getServiciiContabilitate() {
        return serviciiContabilitate;
    }

    public void setServiciiContabilitate(float serviciiContabilitate) {
        this.serviciiContabilitate = serviciiContabilitate;
    }

    public float getMandat() {
        return mandat;
    }

    public void setMandat(float mandat) {
        this.mandat = mandat;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public float getCaldura() {
        return caldura;
    }

    public void setCaldura(float caldura) {
        this.caldura = caldura;
    }

    public float getApaRece() {
        return apaRece;
    }

    public void setApaRece(float apaRece) {
        this.apaRece = apaRece;
    }

    public float getApaCalda() {
        return apaCalda;
    }

    public void setApaCalda(float apaCalda) {
        this.apaCalda = apaCalda;
    }

    @Override
    public String toString() {
        return "General{" +
                "gazeNaturale=" + gazeNaturale +
                ", energieElectrica=" + energieElectrica +
                ", deseuriMenajere=" + deseuriMenajere +
                ", calduraPartiComune=" + calduraPartiComune +
                ", apaPluviala=" + apaPluviala +
                ", serviciiCuratenie=" + serviciiCuratenie +
                ", serviciiContabilitate=" + serviciiContabilitate +
                ", mandat=" + mandat +
                '}';
    }
}
