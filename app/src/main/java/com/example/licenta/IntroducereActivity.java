package com.example.licenta;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.content.FileProvider;

import android.Manifest;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Rect;
import android.media.Image;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.renderscript.ScriptGroup;
import android.util.Log;
import android.util.Patterns;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.licenta.firebase.Callback;
import com.example.licenta.firebase.FirebaseService;
import com.example.licenta.firebase.FirebaseServiceCheltuieliGenerale;
import com.example.licenta.firebase.FirebaseServiceFactura;
import com.example.licenta.firebase.FirebaseServiceIndex;
import com.example.licenta.firebase.FirebaseServiceInformatii;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.mlkit.vision.common.InputImage;
import com.google.mlkit.vision.common.internal.VisionImageMetadataParcel;
import com.google.mlkit.vision.text.Text;
import com.google.mlkit.vision.text.TextRecognition;
import com.google.mlkit.vision.text.TextRecognizer;
import com.theartofdev.edmodo.cropper.CropImage;

import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class IntroducereActivity extends AppCompatActivity {

    private FirebaseServiceIndex firebaseService;


    private FirebaseServiceFactura firebaseService1;
    private FirebaseServiceCheltuieliGenerale firebaseService2;
    private FirebaseServiceInformatii firebaseService3;



    private  static ArrayList<Index> indexes_list=new ArrayList<>() ;
    private  static ArrayList<Factura> bills_list=new ArrayList<>() ;
    private  static ArrayList<General> spends_list=new ArrayList<>() ;
    private  static ArrayList<InformatiiSuplimentare> informations_list=new ArrayList<>() ;


    float sumaDePlata;

    int okFactura=0;

    private Index index=null;
    private Factura factura=null;

    EditText etApaReceBaie;
    EditText etApaReceBucatarie;
    EditText etApaCaldaBaie;
    EditText etApaCaldaBucatarie;
    EditText  etData;

    ImageView camera1;
    ImageView camera2;
    ImageView camera3;
    ImageView camera4;

    String pathToFile;

    Button btnTrimitere;

    int ok1=0;
    int ok2=0;
    int ok3=0;
    int ok4=0;

    float sumaApaRece=0;
    float sumaapaCalda=0;

    float sumaApaRece1=0;
    float sumaapaCalda1=0;


   public static float apaCalda;
  public static  float apaRece;

  public Boolean areCentrala;
  public float  suprafataApartament;

  public static  float sumaRestanta=0 ;


public static int ok=  1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_introducere);


        //initViews
        etApaCaldaBaie=findViewById(R.id.ed_apa_calda_baie);
        etApaReceBaie=findViewById(R.id.ed_apa_rece_baie);
        etApaCaldaBucatarie=findViewById(R.id.ed_apa_calda_bucatarie);
        etApaReceBucatarie=findViewById(R.id.ed_apa_rece_bucatarie);
        etData=findViewById(R.id.et_data);

        btnTrimitere=findViewById(R.id.btn_trimitere);
        camera1=findViewById(R.id.iv_cam1);
        camera2=findViewById(R.id.iv_cam2);
        camera3=findViewById(R.id.iv_cam3);
        camera4=findViewById(R.id.iv_cam4);





        if(Build.VERSION.SDK_INT>=23){
            requestPermissions(new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE},2);

        }
        camera1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ok1=1;
                //CropImage.activity().start(IntroducereActivity.this);
                dispatchPictureTakerAction();
            }
        });

        camera2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ok2=1;
                dispatchPictureTakerAction();
            }
        });
        camera3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ok3=1;
                dispatchPictureTakerAction();
            }
        });
        camera4.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ok4=1;
                dispatchPictureTakerAction();
            }
        });



//        FirebaseDatabase database=FirebaseDatabase.getInstance();
//        DatabaseReference myRef= database.getReference("spends");
//
//        myRef.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                 curatenie = Float.parseFloat(dataSnapshot.child("CURATENIE").getValue().toString());
//                 diverse = Float.parseFloat(dataSnapshot.child("DIVERSE").getValue().toString());
//                 enel = Float.parseFloat(dataSnapshot.child("ENEL").getValue().toString());
//                 gaze = Float.parseFloat(dataSnapshot.child("GAZE").getValue().toString());
//                 lift = Float.parseFloat(dataSnapshot.child("LIFT").getValue().toString());
//                 platiAdmnistrare = Float.parseFloat(dataSnapshot.child("PLATI ADMINISTRARE").getValue().toString());
//                 apaCalda=Float.parseFloat(dataSnapshot.child("APA CALDA").getValue().toString());
//                 apaRece=Float.parseFloat(dataSnapshot.child("APA RECE").getValue().toString());
//
//
//
//
//
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError error) {
//                Log.v("firebase_read", "failed to read value");
//            }
//        });
//


        firebaseService = FirebaseServiceIndex.getInstance();
        firebaseService1= FirebaseServiceFactura.getInstance();
        firebaseService2= FirebaseServiceCheltuieliGenerale.getInstance();
        firebaseService3= FirebaseServiceInformatii.getInstance();




        setupListeners();

        firebaseService.attachDataChangeEventListener(dataChangeEventCallback());
        firebaseService1.attachDataChangeEventListener(dataChangeEventCallback1());
        firebaseService2.attachDataChangeEventListener(dataChangeEventCallback2());
        firebaseService3.attachDataChangeEventListener(dataChangeEventCallback3());








    }

    private void processTextRecognitionResult(Text texts) {
//        List<Text.TextBlock> blocks = texts.getTextBlocks();
//        if (blocks.size() == 0) {
//            Toast.makeText(this, "Niciun text indentificat!", Toast.LENGTH_LONG).show();
//            return;
//        }

        if(texts.getTextBlocks().size()==0){
            Toast.makeText(this, "Niciun text indentificat!", Toast.LENGTH_LONG).show();
            return;
        }

      // mGraphicOverlay.clear();
//        for (int i = 0; i < blocks.size(); i++) {
//            List<Text.Line> lines = blocks.get(i).getLines();
//            for (int j = 0; j < lines.size(); j++)
//                {
//                    List<Text.Element> elements = lines.get(j).getElements();
//                    for (int k = 0; k < elements.size(); k++){
//                        Log.v("pentrunoi", elements.get(k).getText()+"E");
//                    }

//                for (int k = 0; k < elements.size(); k++) {
//                    Graphic textGraphic = new TextGraphic(mGraphicOverlay, elements.get(k));
//                    mGraphicOverlay.add(textGraphic);
                for(Text.TextBlock block : texts.getTextBlocks()){
                    Log.v("pentrunoi", "-->"+block.getText());

            }
        }








    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

//        if(requestCode== CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE){
//            CropImage.ActivityResult result=CropImage.getActivityResult(data);
//
//
//            if(resultCode==RESULT_OK) {
//                    myImageUri=result.getUri();
//                    ivvvvv.setImageURI(myImageUri);
//                if (requestCode == 1) {
//                    Bitmap bitmap = BitmapFactory.decodeFile(pathToFile);
//                    ivvvvv.setImageBitmap(bitmap);
//                }



//            else if(resultCode==CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE){
//                Exception e=result.getError();
//                Toast.makeText(this, "Possible error is: "+e, Toast.LENGTH_SHORT).show();
//            }
       // }

        if(resultCode==RESULT_OK){
            if(requestCode==1){
                Bitmap bitmap = BitmapFactory.decodeFile(pathToFile);
               // ivvvvv.setImageBitmap(bitmap);



                InputImage image = InputImage.fromBitmap(bitmap, 270);
                TextRecognizer recognizer = TextRecognition.getClient();
                Task<Text> result =
                        recognizer.process(image)
                                .addOnSuccessListener(new OnSuccessListener<Text>() {
                                    @Override
                                    public void onSuccess(Text visionText) {
                                       processTextRecognitionResult(visionText);

                                       if(ok1==1) {
                                           float iarb = Float.parseFloat(visionText.getText().toString());
                                       iarb=iarb/1000;
                                       Log.v("pentrunoi","iarb:"+iarb);
                                       etApaReceBaie.setText(String.valueOf(iarb));
                                           ok1=0;}
                                        if(ok2==1) {
                                           float iacb = Float.parseFloat(visionText.getText().toString());
                                           iacb=iacb/1000;
                                           etApaCaldaBaie.setText(String.valueOf(iacb));
                                           ok2=0;}
                                        if(ok3==1) {
                                           float iarbuc = Float.parseFloat(visionText.getText().toString());
                                           iarbuc=iarbuc/1000;
                                           etApaReceBucatarie.setText(String.valueOf(iarbuc));
                                           ok3=0;}
                                         if(ok4==1) {
                                            float  iacbuc = Float.parseFloat(visionText.getText().toString());
                                            iacbuc=iacbuc/1000;
                                            etApaCaldaBucatarie.setText(String.valueOf(iacbuc));
                                            ok4=0;}
                                         

                                }


                                })
                                .addOnFailureListener(
                                        new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                e.printStackTrace();
                                            }
                                        });



            }

        }
    }

    private void dispatchPictureTakerAction(){
      //  CropImage.activity().start(IntroducereActivity.this);
        Intent takePic=new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
         if(takePic.resolveActivity(getPackageManager())!=null){
             File photoFile=null;

             photoFile=createPhotoFile();


             if(photoFile!=null){
                  pathToFile=photoFile.getAbsolutePath();
                 Uri photoURI= FileProvider.getUriForFile(IntroducereActivity.this, "com.example.licenta.fileprovider", photoFile);
                 takePic.putExtra(MediaStore.EXTRA_OUTPUT, photoURI);
                 startActivityForResult(takePic,1);
             }


        }
    }
    private  File createPhotoFile(){
        String name=new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        File storageDir= Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES);
        File image=null;
        try{
             image=File.createTempFile(name,".jpg", storageDir);
        }
        catch (IOException e){
             Log.v("mylog", "Excep: "+e.toString());
        }
        return image;

    }

    private float roundTo3Decs(float value) {
        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(3, RoundingMode.HALF_UP);
        return bd.floatValue();
    }

    private boolean validateContent() {
        if (etApaCaldaBucatarie.getText().toString().isEmpty()) {
            Toast.makeText(this, "Toate indexurile trebuie introduse!", Toast.LENGTH_LONG).show();
            return false;
        }
        if (etApaReceBucatarie.getText().toString().isEmpty()) {
            Toast.makeText(this, "Toate indexurile trebuie introduse!", Toast.LENGTH_LONG).show();
            return false;
        }
        if (etApaCaldaBaie.getText().toString().isEmpty()) {
            Toast.makeText(this, "Toate indexurile trebuie introduse!", Toast.LENGTH_LONG).show();
            return false;
        }
        if (etApaReceBaie.getText().toString().isEmpty()) {
            Toast.makeText(this, "Toate indexurile trebuie introduse!", Toast.LENGTH_LONG).show();
            return false;
        }
        if (etData.getText().toString().isEmpty()) {
            Toast.makeText(this, "Data trebuie introdusa!", Toast.LENGTH_LONG).show();
            return false;
        }

        return true;

    }

    private void setupListeners(){
        btnTrimitere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (LoginTabFragment.numeUtilizator != null) {


                    if (validateContent()) {
                        ok = 1;
                        Index index = new Index();
                        index.setApaCaldaBaie(Float.parseFloat(etApaCaldaBaie.getText().toString()));
                        index.setApaReceBaie(Float.parseFloat(etApaReceBaie.getText().toString()));
                        Log.v("pentrunoi", etApaReceBaie.getText().toString() + "uite l");
                        index.setApaCaldaBucatarie(Float.parseFloat(etApaCaldaBucatarie.getText().toString()));
                        index.setApaReceBucatarie(Float.parseFloat(etApaReceBucatarie.getText().toString()));
                        index.setIdUtilizator(LoginTabFragment.ID);
                        index.setLunaSiAnul(etData.getText().toString());


                        for (InformatiiSuplimentare info : informations_list) {
                            if (info.getIdCont().equals(LoginTabFragment.ID)) {
                                areCentrala = info.getAreCentralaProprie();
                                suprafataApartament = info.getSuprafataUtila();
                            }
                        }


                        for (Factura factura : bills_list) {
                            if (factura.getIdUtilizator().equals(LoginTabFragment.ID)) {

                                okFactura = 1;

                                if (!factura.isPlatita()) {
                                    sumaRestanta = sumaRestanta + Float.parseFloat(factura.getSumaDePlata().split(" ")[0]);
                                }
                            }
                        }

                        if (okFactura == 0) {
                            firebaseService.upsert(index);
                            Toast.makeText(IntroducereActivity.this, "Indexurile au fost trimise cu succes!", Toast.LENGTH_SHORT).show();
                        }


                        for (Index obj : indexes_list) {
                            if (obj.getIdUtilizator().equals(LoginTabFragment.ID)) {
                                sumaapaCalda = obj.getApaCaldaBaie() + obj.getApaCaldaBucatarie();
                                sumaApaRece = obj.getApaReceBaie() + obj.getApaReceBucatarie();


                            }
                        }//la finalul acestui for, sumele vor fi incarcate cu valorile ultimului obiect index introdus

                        sumaapaCalda1 = Float.parseFloat(etApaCaldaBaie.getText().toString()) + Float.parseFloat(etApaCaldaBucatarie.getText().toString());
                        sumaApaRece1 = Float.parseFloat(etApaReceBaie.getText().toString()) + Float.parseFloat(etApaReceBucatarie.getText().toString());


                        Log.v("pentrunoi", index.toString());

                        //firebaseService.upsert(index);

                        //  Toast.makeText(IntroducereActivity.this, "Indexurile au fost trimise cu succes!", Toast.LENGTH_SHORT).show();


                        Log.v("pentrunoi", "ok= " + ok);
                        Log.v("pentrunoi", "sumaapaCalda1: " + sumaapaCalda1 + " sumaapaRece1: " + sumaApaRece1 + " sumaApaCalda: " + sumaapaCalda + " sumaApaRece: " + sumaApaRece);
                        if (sumaapaCalda1 != 0 && sumaApaRece1 != 0 && sumaapaCalda != 0 && sumaApaRece != 0) {
                            float diferentaCald = sumaapaCalda1 - sumaapaCalda;
                            float diferentaRece = sumaApaRece1 - sumaApaRece;


                            for (General general : spends_list) {
                                if (general.getData().equals(index.getLunaSiAnul())) {

                                    Log.v("pentrunoi", "am intrat in for. size: " + spends_list.size());
                                    sumaDePlata = general.getGazeNaturale() + general.getEnergieElectrica() + general.getDeseuriMenajere();
                                    sumaDePlata = sumaDePlata * LoginTabFragment.nrPersoane + diferentaCald * general.getApaCalda() + diferentaRece * general.getApaRece();

                                    float cheltuieliAdministratie = general.getServiciiCuratenie() + general.getServiciiContabilitate() + general.getMandat();
                                    cheltuieliAdministratie = cheltuieliAdministratie * suprafataApartament;

                                    sumaDePlata = sumaDePlata + cheltuieliAdministratie;

                                    if (!areCentrala) {

                                        sumaDePlata = sumaDePlata + general.getCaldura() * suprafataApartament;
                                    }


                                    String dataScadenta = "28/" + etData.getText().toString();

                                    Factura factura = new Factura();
                                    factura.setSumaDePlata(sumaDePlata + " lei");
                                    factura.setDataScadenta(dataScadenta);
                                    factura.setPlatita(false);
                                    factura.setIdUtilizator(LoginTabFragment.ID);
                                    factura.setDiferentaCald(diferentaCald);
                                    factura.setDiferentaRece(diferentaRece);

                                    factura.setIdCheltuieliGenerale(general.getId());


                                    factura.setSumaRestanta(sumaRestanta);
                                    firebaseService.upsert(index);
                                    Toast.makeText(IntroducereActivity.this, "Indexurile au fost trimise cu succes!", Toast.LENGTH_SHORT).show();


                                    Log.v("pentrunoi", "sumarestanta= " + sumaRestanta);
                                    firebaseService1.upsert(factura);
                                    sumaRestanta = 0;
                                } else {
                                    Toast.makeText(IntroducereActivity.this, "Administratorul nu a introdus inca sumele de plata aferente administrarii spatiilor comune. Va rugam reveniti mai tarziu!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        } else {
                            Log.v("pentrunoi", "nu emitem momentan");
                            ok = 0;
                        }


                        Log.v("pentrunoi", index.toString());


                        finish();


                    }
                } else {

                    Toast.makeText(IntroducereActivity.this, "Va rugam introduceti detaliile corespunzatoare contului dumneavoastra pentru ca indexurile sa poata fi trimise cu succes!", Toast.LENGTH_SHORT).show();


                }
            }
        });
    }
    private Callback<List<Index>> dataChangeEventCallback() {
        return new Callback<List<Index>>() {
            @Override
            public void runResultOnUiThread(List<Index> result) {
                if (result != null) {
                    indexes_list.clear();
                    indexes_list.addAll(result);

                }
            }

        };}

        private Callback<List<Factura>> dataChangeEventCallback1() {
            return new Callback<List<Factura>>() {
                @Override
                public void runResultOnUiThread(List<Factura> result) {
                    if (result != null) {
                        bills_list.clear();
                        bills_list.addAll(result);

                    }
                }

            };


    }


    private Callback<List<General>> dataChangeEventCallback2() {
        return new Callback<List<General>>() {
            @Override
            public void runResultOnUiThread(List<General> result) {
                if (result != null) {
                    spends_list.clear();
                    spends_list.addAll(result);

                }
            }

        };


    }
    private Callback<List<InformatiiSuplimentare>> dataChangeEventCallback3() {
        return new Callback<List<InformatiiSuplimentare>>() {
            @Override
            public void runResultOnUiThread(List<InformatiiSuplimentare> result) {
                if (result != null) {
                    informations_list.clear();
                    informations_list.addAll(result);

                }
            }

        };


    }








}