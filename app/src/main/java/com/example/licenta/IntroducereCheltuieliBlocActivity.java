package com.example.licenta;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.licenta.firebase.Callback;
import com.example.licenta.firebase.FirebaseServiceCheltuieliGenerale;
import com.example.licenta.firebase.FirebaseServiceFactura;

import java.util.ArrayList;
import java.util.List;

public class IntroducereCheltuieliBlocActivity extends AppCompatActivity {

    private FirebaseServiceCheltuieliGenerale firebaseService;

    private  static ArrayList<General> spends_list=new ArrayList<>() ;


    EditText etGazeNaturale;
    EditText etEnergieElectrica;
    EditText etDeseuriMenajere;
    EditText etCalduraPartiComune;
    EditText  etServiciiCuratenie;
    EditText etServiciiContabilitate;
    EditText etMandat;
    EditText etApaPluviala;
    EditText etCaldura;
    EditText etApaCalda;
    EditText etApaRece;



    EditText etData;



    ImageView btnTrimitere;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_introducere_cheltuieli_bloc);

        etGazeNaturale=findViewById(R.id.et_gaze_naturale);
        etEnergieElectrica=findViewById(R.id.et_energie_electrica);
        etDeseuriMenajere=findViewById(R.id.et_deseuri);
        etCalduraPartiComune=findViewById(R.id.et_caldura_parti_comune);
        etServiciiCuratenie=findViewById(R.id.et_curatenie);
        etServiciiContabilitate=findViewById(R.id.et_contabilitate);
        etMandat=findViewById(R.id.et_mandat);
        etApaPluviala=findViewById(R.id.et_apa_pluviala);
        etData=findViewById(R.id.et_data_cheltuieli_generale);
        etCaldura=findViewById(R.id.et_caldura) ;
        etApaCalda=findViewById(R.id.et_apa_calda) ;
        etApaRece=findViewById(R.id.et_apa_rece) ;




        btnTrimitere=findViewById(R.id.btn_trimite_cheltuieli_generale);

        firebaseService= FirebaseServiceCheltuieliGenerale.getInstance();

        setupListeners();

        firebaseService.attachDataChangeEventListener(dataChangeEventCallback());




    }


    private void setupListeners(){
        btnTrimitere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateContent()) {

                    General general = new General();


                    general.setGazeNaturale(Float.parseFloat(etGazeNaturale.getText().toString()));
                    general.setEnergieElectrica(Float.parseFloat(etEnergieElectrica.getText().toString()));
                    general.setDeseuriMenajere(Float.parseFloat(etDeseuriMenajere.getText().toString()));
                    general.setCalduraPartiComune(Float.parseFloat(etCalduraPartiComune.getText().toString()));
                    general.setServiciiCuratenie(Float.parseFloat(etServiciiCuratenie.getText().toString()));
                    general.setServiciiContabilitate(Float.parseFloat(etServiciiContabilitate.getText().toString()));
                    general.setMandat(Float.parseFloat(etMandat.getText().toString()));
                    general.setData(etData.getText().toString());
                    general.setCaldura(Float.parseFloat(etCaldura.getText().toString())) ;
                    general.setApaCalda(Float.parseFloat(etApaCalda.getText().toString())); ;
                    general.setApaRece(Float.parseFloat(etApaRece.getText().toString())); ;


                    general.setApaPluviala(Float.parseFloat(etApaPluviala.getText().toString()));




                    firebaseService.upsert(general);

                }


                Toast.makeText(IntroducereCheltuieliBlocActivity.this, "Trimiterea a fost efectuata cu succes!", Toast.LENGTH_SHORT).show();


                finish();


            }

        });
    }

    private boolean validateContent() {
        if (etCalduraPartiComune.getText().toString().isEmpty()) {
            Toast.makeText(this, "Toate indexurile trebuie introduse!", Toast.LENGTH_LONG).show();
            return false;
        }
        if (etData.getText().toString().isEmpty()) {
            Toast.makeText(this, "Toate indexurile trebuie introduse!", Toast.LENGTH_LONG).show();
            return false;
        }
        if (etDeseuriMenajere.getText().toString().isEmpty()) {
            Toast.makeText(this, "Toate indexurile trebuie introduse!", Toast.LENGTH_LONG).show();
            return false;
        }
        if (etEnergieElectrica.getText().toString().isEmpty()) {
            Toast.makeText(this, "Toate indexurile trebuie introduse!", Toast.LENGTH_LONG).show();
            return false;
        }
        if (etGazeNaturale.getText().toString().isEmpty()) {
            Toast.makeText(this, "Data trebuie introdusa!", Toast.LENGTH_LONG).show();
            return false;
        }
        if (etMandat.getText().toString().isEmpty()) {
            Toast.makeText(this, "Data trebuie introdusa!", Toast.LENGTH_LONG).show();
            return false;
        } if (etServiciiContabilitate.getText().toString().isEmpty()) {
            Toast.makeText(this, "Data trebuie introdusa!", Toast.LENGTH_LONG).show();
            return false;
        } if (etServiciiCuratenie.getText().toString().isEmpty()) {
            Toast.makeText(this, "Data trebuie introdusa!", Toast.LENGTH_LONG).show();
            return false;
        }




        return true;

    }


    private Callback<List<General>> dataChangeEventCallback() {
        return new Callback<List<General>>() {
            @Override
            public void runResultOnUiThread(List<General> result) {
                if (result != null) {
                    spends_list.clear();
                    spends_list.addAll(result);

                }
            }

        };


    }

}