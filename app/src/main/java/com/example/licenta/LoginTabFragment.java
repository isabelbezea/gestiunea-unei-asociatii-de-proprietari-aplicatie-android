package com.example.licenta;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.licenta.firebase.Callback;
import com.example.licenta.firebase.FirebaseService;
import com.example.licenta.firebase.FirebaseServiceCheltuieliGenerale;
import com.example.licenta.firebase.FirebaseServiceInformatii;

import java.util.ArrayList;
import java.util.List;

public class LoginTabFragment extends Fragment {

    Button btnLogin;
    EditText etUsername;
    EditText etPassword;

    TextView tvParola;

    FirebaseService firebaseService;
    public static ArrayList<User> users_list = new ArrayList<>();
    private int ok=0;
    public static String ID;
    public static Boolean OKadministrator=false;
    public static int nrPersoane;
    public static String email;
    public static int nrApartament;
    public static float suprafataUtila;
    public static String numeUtilizator;
    public static String prenumeUtilizator;
    public static String nrTelefon;
    public static String tipLocatar;
    public static Boolean areCentralaProprie;





    private FirebaseServiceInformatii firebaseService2;
    private  static ArrayList<InformatiiSuplimentare> informations_list=new ArrayList<>() ;

    private FirebaseServiceCheltuieliGenerale firebaseServiceCheltuieliGenerale;
    public  static ArrayList<General> spends_list=new ArrayList<>() ;




    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        ViewGroup root=(ViewGroup) inflater.inflate(R.layout.login_tab_fragment, container, false);

        etUsername=root.findViewById(R.id.et_username);
        etPassword=root.findViewById(R.id.et_password);
        btnLogin=root.findViewById(R.id.btn_login);

        tvParola=root.findViewById(R.id.tv_parola);
//        btnLogin.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Intent intent=new Intent(getActivity(), Dashboard.class);
//                startActivity(intent);
//            }
//        });

        firebaseService=FirebaseService.getInstance();
        firebaseService.attachDataChangeEventListener(dataSelectEventCallback());

        firebaseService2= FirebaseServiceInformatii.getInstance();
        firebaseService2.attachDataChangeEventListener(dataChangeEventCallback2());

        firebaseServiceCheltuieliGenerale = FirebaseServiceCheltuieliGenerale.getInstance();
        firebaseServiceCheltuieliGenerale.attachDataChangeEventListener(dataChangeEventCallback3());




        btnLogin.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {


                    String username = etUsername.getText().toString();
                    String password = etPassword.getText().toString();
                    Log.v("pentrunooi", username + "........");
                    Log.v("pentrunooi", users_list.size()+".");

                    for (User user : users_list) {
                        Log.v("pentrunooi", user.getNumeUtilizator());
                        if (username.equals(user.getNumeUtilizator()) && password.equals(user.getParola())) {
                            ok = 1;
                            ID=user.getId();
                            if(user.getNumeUtilizator().equals("administrator")) OKadministrator=true;
                            Log.v("penturnoi", "okadm"+OKadministrator);

                            nrPersoane=user.getNrPersoane();
                            email=user.getEmail();
                            nrApartament=user.getNrApartament();



                            Log.v("pentrunooi", ID);
                            Log.v("pentrunooi", ok+"!!!!!!");
                            break;
                        }

                    }

                    for(InformatiiSuplimentare informatiiSuplimentare:informations_list){
                        if(informatiiSuplimentare.getIdCont().equals(ID)){
                            suprafataUtila=informatiiSuplimentare.getSuprafataUtila() ;
                            numeUtilizator=informatiiSuplimentare.getNume() ;
                            prenumeUtilizator=informatiiSuplimentare.getPrenume();
                            nrTelefon=informatiiSuplimentare.getNumarTelefon() ;
                            tipLocatar=informatiiSuplimentare.getTipLocatar();
                            areCentralaProprie=informatiiSuplimentare.getAreCentralaProprie();
                            Log.v("pentrunoi", "s-a rezolvat? "+suprafataUtila) ;
                        }
                    }
                    Log.v("pentrunooi", ok + "....");
                    if (ok == 1) {

                        if(OKadministrator==false){

                        Intent intent = new Intent(getActivity(), Dashboard.class);
                        startActivity(intent); }

                        else {
                            Intent intent = new Intent(getActivity(), DashboardActivity.class);
                            startActivity(intent);
                        }

                    } else
                        Toast.makeText(getContext(), "Nume de utilizator sau parola introduse gresit!", Toast.LENGTH_SHORT).show();

                }

            });

            tvParola.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                   Intent intent = new Intent(getActivity(), RecuperareParola.class);
                   startActivity(intent);

                }
            });

        return root;
    }
    private Callback<List<User>> dataSelectEventCallback() {
        return new Callback<List<User>>() {
            @Override
            public void runResultOnUiThread(List<User> result) {
                if (result != null) {
                    users_list.clear();
                    users_list.addAll(result);
                }
            }

        };
    }

    private Callback<List<InformatiiSuplimentare>> dataChangeEventCallback2() {
        return new Callback<List<InformatiiSuplimentare>>() {
            @Override
            public void runResultOnUiThread(List<InformatiiSuplimentare> result) {
                if (result != null) {
                    informations_list.clear();
                    informations_list.addAll(result);

                }
            }

        };


    }


    private Callback<List<General>> dataChangeEventCallback3() {
        return new Callback<List<General>>() {
            @Override
            public void runResultOnUiThread(List<General> result) {
                if (result != null) {
                    spends_list.clear();
                    spends_list.addAll(result);

                }
            }

        };


    }



}
