package com.example.licenta;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.firebase.database.DatabaseReference;

import java.util.ArrayList;

public class DocumentAdapter extends BaseAdapter {

    private ArrayList<PDF> pdfuri;
    private Context context;
    private LayoutInflater inflater;

    DatabaseReference databaseReference;
    public static ArrayList<Contact> contacts_list = new ArrayList<>();

    public DocumentAdapter(ArrayList<PDF> pdfuri, Context context) {
        this.pdfuri = pdfuri;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return pdfuri.size();
    }

    @Override
    public Object getItem(int position) {
        return pdfuri.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {


        final View pdfView=inflater.inflate(R.layout.pdf_item, viewGroup, false);

        final TextView tvNumeDocument =pdfView.findViewById(R.id.tv_nume_document);
        final TextView tvDescriereDocument=pdfView.findViewById(R.id.tv_descriere_document);


        final TextView tvUrl=pdfView.findViewById(R.id.tv_url);



        PDF pdf=pdfuri.get(position);//obiectul pe care se da click
        tvNumeDocument.setText(pdf.getDenumire());
        tvDescriereDocument.setText(pdf.getDescriere());
        tvUrl.setText(pdf.getUrl());


        return pdfView;
    }

    public void removeElement(int position)
    {
        pdfuri.remove(position);
        //metoda asta ii zice adaptorului ca s a modificat continutul listei  => tb sa recreeze elem
        notifyDataSetChanged();

    }

}
