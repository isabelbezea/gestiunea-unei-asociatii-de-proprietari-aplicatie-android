package com.example.licenta;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.os.Handler;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.craftman.cardform.Card;
import com.craftman.cardform.CardForm;
import com.craftman.cardform.OnPayBtnClickListner;
import com.example.licenta.firebase.Callback;
import com.example.licenta.firebase.FirebaseService;
import com.example.licenta.firebase.FirebaseServiceFactura;

import java.util.ArrayList;
import java.util.List;

public class CardActivity extends AppCompatActivity {

    EditText etNumarCard;
    EditText etDataExpirare;
    EditText etCVV;

    ImageView ivCard;
    ImageView ivCheck1;
    ImageView ivCheck2;

    TextView tvNumarCard;
   TextView tvDataExpirare;
    TextView tvLuna;
    TextView tvAnul;
    TextView tvCVV;
    Button btnPlateste;
    Boolean mEditing = false;
    Boolean mEditingD = false;
    Boolean mEditingD1 = false;
    Boolean mEditingN = false;

    Dialog dialog ;
    Dialog dialog1 ;

    String LUNA;
    String ANUL;

    public int save;
    public int ok= 0;

    ArrayList<String> arrayListLuni;
    ArrayList<String> arrayListAni;
    FirebaseServiceFactura firebaseServiceFactura;
    public static ArrayList<Factura> facturi_list = new ArrayList<>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_card);

        firebaseServiceFactura = FirebaseServiceFactura.getInstance();
        firebaseServiceFactura.attachDataChangeEventListener(dataSelectEventCallback());

        etNumarCard = findViewById(R.id.ed_numar_card);
       //etDataExpirare = findViewById(R.id.ed_data_expirare);
        etCVV = findViewById(R.id.ed_cvv);
        tvLuna = findViewById(R.id.tv_luna);
        tvAnul = findViewById(R.id.tv_anul);




        ivCard = findViewById(R.id.iv_card);
        ivCheck1 = findViewById(R.id.iv_check1);
        ivCheck2 = findViewById(R.id.iv_check2);

        tvNumarCard = findViewById(R.id.tv_numar_card);
        tvDataExpirare = findViewById(R.id.tv_data_expirare);
        tvCVV = findViewById(R.id.tv_cvv);
        btnPlateste = findViewById(R.id.btn_plateste_final);

        arrayListLuni=new ArrayList<>();
        arrayListAni=new ArrayList<>();


        for(int i=1;i<=9;i++){
            arrayListLuni.add("0"+i);

        }
      for(int i=10;i<=12;i++){
          arrayListLuni.add(i+"");

      }

      for(int i=2021;i<=2050;i++){
          arrayListAni.add(i+"");
      }

        for (int i=0;i<arrayListLuni.size() ;i++) {
            Log.v("pentrunoi", "lista: "+arrayListLuni.get(i));
        }

      tvLuna.setOnClickListener(new View.OnClickListener() {
          @Override
          public void onClick(View v) {
              dialog=new Dialog(CardActivity.this);
              dialog.setContentView(R.layout.dialog_searchable_spinner);

              dialog.getWindow().setLayout(650,800);

              dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

              dialog.show();

              EditText editText=dialog.findViewById(R.id.edit_text);
              ListView listView=dialog.findViewById(R.id.list_view);

              final ArrayAdapter<String> adapter=new ArrayAdapter<>(CardActivity.this, android.R.layout.simple_list_item_1, arrayListLuni);


              listView.setAdapter(adapter);
              editText.addTextChangedListener(new TextWatcher() {
                  @Override
                  public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                  }

                  @Override
                  public void onTextChanged(CharSequence s, int start, int before, int count) {
                    adapter.getFilter().filter(s);

                  }

                  @Override
                  public void afterTextChanged(Editable s) {

                }


              });
              listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                  @Override
                  public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                      tvLuna.setText(adapter.getItem(position));
//                        if(tvLuna.getText().toString().length()!=2){
//                            ivCard.setImageResource(R.drawable.exclamation);
//                            ivCard.setVisibility(View.VISIBLE);
//                            Toast.makeText(CardActivity.this, "Nu a fost selectata nicio luna!", Toast.LENGTH_SHORT).show();
//                        }
//                        else {

                      LUNA = tvLuna.getText().toString();
                      Log.v("pentrunoi", LUNA);

                      if (tvAnul.getText().toString().length() == 4) {
                          Log.v("pentrunoi", "vaca mu");

                          tvDataExpirare.setText("Data expirare: " + LUNA + "/" + ANUL);
                          ivCheck1.setImageResource(R.drawable.ic_baseline_check_24);
                          ivCheck1.setVisibility(View.VISIBLE);

                      }

                      dialog.dismiss();
                  }
              });
          }
      });
        tvAnul.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog1 = new Dialog(CardActivity.this);
                dialog1.setContentView(R.layout.dialog_searchable_spinner1);

                dialog1.getWindow().setLayout(650, 800);

                dialog1.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

                dialog1.show();

                EditText editText1 = dialog1.findViewById(R.id.edit_text1);
                ListView listView1 = dialog1.findViewById(R.id.list_view1);

                final ArrayAdapter<String> adapter1 = new ArrayAdapter<>(CardActivity.this, android.R.layout.simple_list_item_1, arrayListAni);


                listView1.setAdapter(adapter1);
                editText1.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {
                        adapter1.getFilter().filter(s);

                    }

                    @Override
                    public void afterTextChanged(Editable s) {

                    }
                });
                listView1.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                    @Override
                    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                        tvAnul.setText(adapter1.getItem(position));


                        ANUL = tvAnul.getText().toString().substring(2, 4);
                        if (tvLuna.getText().toString().length() == 2) {
                            {
                                Log.v("pentrunoi", "VACA  MU");
                                tvDataExpirare.setText("Data expirare: " + LUNA + "/" + ANUL);
                                ivCheck1.setImageResource(R.drawable.ic_baseline_check_24);
                                ivCheck1.setVisibility(View.VISIBLE);

                            }
                        }


                        dialog1.dismiss();
                    }


                });
//                if (tvAnul.getText().toString().length() != 4) {
//
//                    ivCheck1.setImageResource(R.drawable.exclamation);
//                    ivCheck1.setVisibility(View.VISIBLE);
//                    Toast.makeText(CardActivity.this, "Nu a fost selectat niciun an!", Toast.LENGTH_SHORT).show();
//                }

            }

        });

        etNumarCard.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!mEditingN && etNumarCard.hasFocus()) {
                    mEditingN = true;

                }
            }
        });

        etNumarCard.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {


                if (!hasFocus && mEditingN) {

                    mEditingN = false;


                    Log.v("pentrunoi", etNumarCard.getText().toString().length() + "L");
                    if (etNumarCard.getText().toString().length() == 16) {
                        Log.v("pentrunoi", "if 1");
                        tvNumarCard.setText(etNumarCard.getText().toString().substring(0, 4) + " " + etNumarCard.getText().toString().substring(4, 8) + " " + etNumarCard.getText().toString().substring(8, 12) + " " + etNumarCard.getText().toString().substring(12, 16));


                        if (etNumarCard.getText().toString().charAt(0) == '4') {
                            Log.v("pentrunoi", "if 2");
                            ivCard.setImageResource(R.drawable.visa);
                            ivCard.setVisibility(View.VISIBLE);

                        } else if (etNumarCard.getText().toString().charAt(0) == '5') {
                            Log.v("pentrunoi", "if 3");
                            ivCard.setImageResource(R.drawable.mastercard);
                            ivCard.setVisibility(View.VISIBLE);


                        } else {
                            ivCard.setImageResource(R.drawable.exclamation);
                            ivCard.setVisibility(View.VISIBLE);
                            Toast.makeText(CardActivity.this, "Numarul de card nu este valid", Toast.LENGTH_SHORT).show();
                        }
                    } else {
                        ivCard.setImageResource(R.drawable.exclamation);
                        ivCard.setVisibility(View.VISIBLE);
                        Toast.makeText(CardActivity.this, "Numarul de card nu este valid", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });



        etCVV.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (!mEditing && etCVV.hasFocus()) {
                    mEditing = true;
                    Log.v("pentrunoi", mEditing.toString());
                }
            }
        });


        etCVV.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View v, boolean hasFocus) {


                if (!hasFocus && mEditing) {

                    mEditing = false;
                    Log.v("pentrunoi", mEditing.toString());

                    Log.v("pentrunoi", etCVV.getText().toString().length() + "!");
                    if (etCVV.getText().toString().length() == 3) {

                        tvCVV.setText("CVV: " + etCVV.getText().toString());


                        ivCheck2.setImageResource(R.drawable.ic_baseline_check_24);
                        ivCheck2.setVisibility(View.VISIBLE);


                    } else {
                        ivCheck2.setImageResource(R.drawable.exclamation);
                        ivCheck2.setVisibility(View.VISIBLE);
                        Toast.makeText(CardActivity.this, "CVV invalid", Toast.LENGTH_SHORT).show();
                    }


                }

            }

        });
    saveLastOpenField();
        final ProcesareDialog procesareDialog = new ProcesareDialog(CardActivity.this);


        btnPlateste.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(validate()) {
                    for (Factura factura : facturi_list) {
                        if (FacturiAdapter.idFactura.equals(factura.getId())) {
                            factura.setPlatita(true);
                            firebaseServiceFactura.upsert(factura);

                        }
                    }



                    procesareDialog.startProcesareDialog();
                    Handler handler = new Handler();
                    handler.postDelayed(new Runnable() {
                        @Override
                        public void run() {
                            procesareDialog.dismissDialog();

                            Intent intent = new Intent(CardActivity.this, PlataEfectActivity.class);
                            startActivity(intent);
                        }
                    }, 4000);
                }
            }

        });
    }

    protected void saveLastOpenField() {
        ArrayList<EditText> fields = new ArrayList<>();
        ArrayList<TextView> fields2 = new ArrayList<>();
        fields.add(etNumarCard);
        fields.add(etCVV);
        for (EditText view : fields) {
            view.clearFocus();
        }

        fields2.add(tvLuna);
        fields2.add(tvAnul);
        for (TextView view : fields2) {
            view.clearFocus();
        }
    }

    private Callback<List<Factura>> dataSelectEventCallback() {
        return new Callback<List<Factura>>() {
            @Override
            public void runResultOnUiThread(List<Factura> result) {
                if (result != null) {
                    facturi_list.clear();

                    for (Factura factura : result) {

                        if (factura.getIdUtilizator().equals(LoginTabFragment.ID)) {

                            facturi_list.add(factura);
                        }

                    }
                }

            }


        };
    }

    private boolean validate(){
        if (etCVV.getText().toString().length() != 3) {
            Toast.makeText(CardActivity.this, "CVV invalid", Toast.LENGTH_SHORT).show();
            return false;
        }
        if ((etNumarCard.getText().toString().length() != 16)
        ||(etNumarCard.getText().toString().charAt(0) != '4')
            && (etNumarCard.getText().toString().charAt(0) != '5')) {

                Toast.makeText(CardActivity.this, "Numarul de card nu este valid", Toast.LENGTH_SHORT).show();
        return  false;}

        if(tvLuna.getText().toString().length()!=2){
            Toast.makeText(CardActivity.this, "Luna trebuie introdusa!", Toast.LENGTH_SHORT).show();
            return   false;
        }
        if(tvAnul.getText().toString().length()!=4){
            Toast.makeText(CardActivity.this, "Anul trebuie introdus!", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;

    }
}





