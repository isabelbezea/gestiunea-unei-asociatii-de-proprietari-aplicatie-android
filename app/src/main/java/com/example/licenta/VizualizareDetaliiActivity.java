package com.example.licenta;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.licenta.firebase.Callback;
import com.example.licenta.firebase.FirebaseService;
import com.example.licenta.firebase.FirebaseServiceContact;
import com.example.licenta.firebase.FirebaseServiceFactura;
import com.example.licenta.firebase.FirebaseServiceInformatii;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class VizualizareDetaliiActivity extends AppCompatActivity {



    TextView tvNumePrenume;
    TextView tvTipLocatar;
    TextView tvNrTelefon;
    TextView tvAdresaEmail;
    TextView tvNrApartament;
    TextView tvSuprafataUtila;
    TextView tvNrPersoane;
    TextView tvCentrala;
    TextView tvSituatieFacturi;

    FirebaseServiceInformatii firebaseServiceInformatii;
    FirebaseService firebaseServiceUser;

    public static ArrayList<InformatiiSuplimentare> informatiiSuplimentares_list = new ArrayList<>();
    public static ArrayList<User> users_list = new ArrayList<>();

    public static    String id;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vizualizare_detalii);

        tvNumePrenume = findViewById(R.id.tv_nume_prenume_detalii);
        tvTipLocatar = findViewById(R.id.tv_tip_locatar_detalii);
        tvNrTelefon = findViewById(R.id.tv_nr_telefon_detalii);
        tvAdresaEmail = findViewById(R.id.tv_email_detalii);
        tvNrApartament = findViewById(R.id.tv_nr_apartament_detaliii);
        tvSuprafataUtila = findViewById(R.id.tv_suprafata_utila_detalii);
        tvNrPersoane = findViewById(R.id.tv_nr_persoane_detalii);
        tvCentrala = findViewById(R.id.tv_are_centrala_detalii);
        tvSituatieFacturi=findViewById(R.id.tv_situatie_facturi_detalii);


        firebaseServiceInformatii = FirebaseServiceInformatii.getInstance();
        firebaseServiceInformatii.attachDataChangeEventListener(dataChangeEventCallback());

        firebaseServiceUser = FirebaseService.getInstance();
        firebaseServiceUser.attachDataChangeEventListener(dataChangeEventCallback1());

        String nume, prenume;
        nume=ManagingAdapter.numePrenumeT.split(" ")[0];
        prenume=ManagingAdapter.numePrenumeT.split(" ")[1];




        for(InformatiiSuplimentare informatiiSuplimentare: informatiiSuplimentares_list) {
            if (informatiiSuplimentare.getNume().equals(nume) && informatiiSuplimentare.getPrenume().equals(prenume)) {
                id=informatiiSuplimentare.getIdCont();
                tvTipLocatar.setText(informatiiSuplimentare.getTipLocatar());
                tvNumePrenume.setText(ManagingAdapter.numePrenumeT);
                tvNrTelefon.setText(informatiiSuplimentare.getNumarTelefon());
                tvSuprafataUtila.setText(String.valueOf(informatiiSuplimentare.getSuprafataUtila()));
                if (informatiiSuplimentare.getAreCentralaProprie()) {
                    tvCentrala.setText("DA");
                } else {
                    tvCentrala.setText("NU");
                }
            }
        }

        for(User user: users_list){
            if(user.getId().equals(id)){

                tvAdresaEmail.setText(user.getEmail());
                tvNrApartament.setText(String.valueOf(user.getNrApartament()));
                tvNrPersoane.setText(String.valueOf(user.getNrPersoane()));
            }
        }

        tvSituatieFacturi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(VizualizareDetaliiActivity.this, FacturiListActivity.class);
                startActivity(intent);
            }
        });





            }




        private Callback<List<InformatiiSuplimentare>> dataChangeEventCallback() {
            return new Callback<List<InformatiiSuplimentare>>() {
                @Override
                public void runResultOnUiThread(List<InformatiiSuplimentare> result) {
                    if (result != null) {
                        informatiiSuplimentares_list.clear();
                        informatiiSuplimentares_list.addAll(result);
                    }
                                   }

            }
                    ;

        }
        private Callback<List<User>> dataChangeEventCallback1() {
            return new Callback<List<User>>() {
                @Override
                public void runResultOnUiThread(List<User> result) {
                    if (result != null) {
                        users_list.clear();
                        users_list.addAll(result);
                    }                }

            }
                    ;

        }






}