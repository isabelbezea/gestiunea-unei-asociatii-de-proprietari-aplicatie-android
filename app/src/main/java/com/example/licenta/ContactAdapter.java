package com.example.licenta;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.telephony.SmsManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.licenta.firebase.FirebaseServiceContact;
import com.example.licenta.firebase.FirebaseServiceFactura;
import com.example.licenta.firebase.FirebaseServiceInformatii;

import java.util.ArrayList;

public class ContactAdapter extends BaseAdapter {


    private ArrayList<Contact> contacte;
    private Context context;
    private LayoutInflater inflater;

    int selectedPosition=-1;

    int ok=0;


    FirebaseServiceContact firebaseService;
    public static ArrayList<Contact> contacts_list = new ArrayList<>();

    public ContactAdapter(ArrayList<Contact> contacte, Context context) {
        this.contacte = contacte;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return contacte.size();
    }

    @Override
    public Object getItem(int position) {
        return contacte.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {
        final View contactView=inflater.inflate(R.layout.contact_item, viewGroup, false);

        final TextView tvNume =contactView.findViewById(R.id.tv_nume);
        final TextView tvFunctie=contactView.findViewById(R.id.tv_functie);
        final TextView tvNrTelefon=contactView.findViewById(R.id.tv_telefon);


        final ImageView ivCall=contactView.findViewById(R.id.iv_call);
        final ImageView ivMessage=contactView.findViewById(R.id.iv_message);

        final LinearLayout llContactItem=contactView.findViewById(R.id.ll_contact_item);



        Contact contact=contacte.get(position);//obiectul pe care se da click
        tvNume.setText(contact.getNume());
        tvFunctie.setText(contact.getFunctie());
        tvNrTelefon.setText(contact.getNumarTelefon());


        llContactItem.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(ok==0){
                    ok=1; //expandam
                    ivCall.setVisibility(View.VISIBLE);
                    ivMessage.setVisibility(View.VISIBLE);
                }
                else{
                    ok=0; //inchidem expandarea
                    ivCall.setVisibility(View.GONE);
                    ivMessage.setVisibility(View.GONE);

                }
            }
        });

        ivMessage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                Intent intent = new Intent(Intent.ACTION_MAIN);
//                intent.addCategory(Intent.CATEGORY_APP_MESSAGING);
//                context.startActivity(intent);

                Log.v("pentrunoi", contact.getNumarTelefon()+".");
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse("sms:"+contact.getNumarTelefon()));

              //  intent.setData(Uri.parse("smsto:")); // only email apps should handle this
              //  intent.putExtra(Intent.EXTRA_PHONE_NUMBER, new String[]{"0731658973"});
                intent.putExtra("sms_body", "Buna ziua! Cand aveti disponibilitate, as dori sa stabilim un apel! Multumesc!");
                if (intent.resolveActivity(context.getPackageManager()) != null) {
                    context.startActivity(intent);
                }
            }

        });

        ivCall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Intent.ACTION_DIAL);
              //  intent.addCategory(Intent.ACTION_DIAL);
               intent.setData(Uri.parse("tel:"+contact.getNumarTelefon()));

                context.startActivity(intent);
            }
        });


        final Contact contact1=contacte.get(position);
        selectedPosition=position;

        llContactItem.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {

                Intent intent=new Intent( context,  AdaugaContactActivity.class);
                intent.putExtra("numeContact", contact.getNume());
                intent.putExtra("functieContact", contact.getFunctie());
                intent.putExtra("nrTelefonContact", contact.getNumarTelefon());

                context.startActivity(intent);


                return false;
            }
        });





        return contactView;
    }

    public void addElements(ArrayList<Contact> list)
    {
        contacte.addAll(list);
        notifyDataSetChanged();
    }

    public void  removeElement(int position)
    {
        contacte.remove(position);
        notifyDataSetChanged();
    }

}
