package com.example.licenta;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.pdf.PdfDocument;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.core.app.ActivityCompat;

import com.example.licenta.firebase.FirebaseServiceFactura;
import com.muddzdev.styleabletoastlibrary.StyleableToast;

import org.w3c.dom.Text;

import java.util.ArrayList;

public class FacturiAdapter extends BaseAdapter {
    private ArrayList<Factura> facturi;
    private Context context;
    private LayoutInflater inflater;

    public static String idFactura;
    public static String dataScadenta;
    public static String sumaDePlata;

    public static float difCald;
    public  static  float  difRece;
    public int ok=0;

    Dialog dialog;

    public static General generalTransmis=new General();


FirebaseServiceFactura  firebaseServiceFactura;
    public static ArrayList<Factura> facturi_list = new ArrayList<>();


    public FacturiAdapter(ArrayList<Factura> facturi, Context context) {
        this.facturi= facturi;
        this.context = context;
        this.inflater = LayoutInflater.from(context);
    }

    @Override
    public int getCount() {
        return facturi.size();
    }

    @Override
    public Object getItem(int position) {
        return facturi.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, final View convertView, ViewGroup viewGroup) {
        final View facturaView=inflater.inflate(R.layout.factura_item, viewGroup, false);
        final TextView tvIdV =facturaView.findViewById(R.id.tv_id_v);
        final TextView tvDataV=facturaView.findViewById(R.id.tv_data_v);
        final TextView tvSumaV=facturaView.findViewById(R.id.tv_suma_v);
         Button btnPlateste=facturaView.findViewById(R.id.btn_plateste);
         Button btnDescarca=facturaView.findViewById(R.id.btn_descarca_factura);

        ImageView ivWarn=facturaView.findViewById(R.id.iv_warn) ;


        Factura factura=facturi.get(position);//obiectul pe care se da click
        tvIdV.setText(factura.getId());
        tvDataV.setText(factura.getDataScadenta());
        tvSumaV.setText(factura.getSumaDePlata());

        Log.v("pentrunoi", FacturiListActivity.spends_list.size()+":(");
        for(General obj: LoginTabFragment.spends_list){
            if(obj.getId().equals(factura.getIdCheltuieliGenerale())){
                generalTransmis.setId(obj.getId());
                generalTransmis.setGazeNaturale(obj.getGazeNaturale());
                generalTransmis.setEnergieElectrica(obj.getEnergieElectrica());
                generalTransmis.setDeseuriMenajere(obj.getDeseuriMenajere());
                generalTransmis.setCaldura(obj.getCaldura());
                generalTransmis.setApaPluviala(obj.getApaPluviala());
                generalTransmis.setServiciiCuratenie(obj.getServiciiCuratenie());
                generalTransmis.setServiciiContabilitate(obj.getServiciiContabilitate());
                generalTransmis.setMandat(obj.getMandat());
                generalTransmis.setData(obj.getData());
                generalTransmis.setCalduraPartiComune(obj.getCalduraPartiComune());
                generalTransmis.setApaRece(obj.getApaRece());
                generalTransmis.setApaCalda(obj.getApaCalda());
            }
        }
        Log.v("pentrunoi", "aici2: "+generalTransmis.toString());



        if(LoginTabFragment.OKadministrator){
            btnPlateste.setVisibility(View.GONE);
            btnDescarca.setVisibility(View.GONE);
        }

        if(factura.isPlatita()==true){
            btnPlateste.setVisibility(View.INVISIBLE);


        }

        if(factura.getSumaRestanta()!=0 && factura.isPlatita()==false){
            Log.v("pentrunoi", "YES");
            ivWarn.setVisibility(View.VISIBLE);
        }


        ivWarn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                StyleableToast.makeText(  context, "Facturi restante! Suma de plata: "+ factura.getSumaRestanta()+" lei. ", R.style.exampleToast).show();
            }
        });

        btnPlateste.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                idFactura=tvIdV.getText().toString();

                for(int i=position+1; i<facturi.size();i++){
                    if( !facturi.get(i).isPlatita()){
                        Toast.makeText(context , "Pentru a plati aceasta factura, mai intai trebuie platite cele anterioare!", Toast.LENGTH_SHORT).show();
                        ok=1;
                        break;
                    }
                }

                if(ok!=1) {
                    Intent intent=new Intent(context, CardActivity.class);
                    context.startActivity( intent);



                }

                // ivWarn.setVisibility(View.INVISIBLE);



                ok=0;

            }
        });

        btnDescarca.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


                idFactura=tvIdV.getText().toString();
                dataScadenta=tvDataV.getText().toString();
                sumaDePlata=tvSumaV.getText().toString();
                for(Factura factura1 : facturi) {
                    if(factura1.getId().equals(idFactura)) {
                        Log.v("pentrunoi", "facura.getid: "+factura1.getId());

                        difCald=factura1.getDiferentaCald();
                        difRece=factura1.getDiferentaRece();
                        Log.v("pentrunoi", "!!!! BUN: "+difCald+"; "+difRece);
                    }}

                Intent intent=new Intent(context, PdfActivity2.class);
                context.startActivity( intent);
            }
        });


        return facturaView;

    }


    public void addElements(ArrayList<Factura> list)
    {
        facturi.addAll(list);
        notifyDataSetChanged();
    }

    public void  removeElement(int position)
    {
        facturi.remove(position);
        notifyDataSetChanged();
    }

}
