package com.example.licenta;

import android.app.Activity;
import android.app.AlertDialog;
import android.view.LayoutInflater;

public class ProcesareDialog {

   private Activity activity;
   private AlertDialog dialog;

    public ProcesareDialog(Activity  activity) {
        this.activity=activity;
    }

    void startProcesareDialog(){
        AlertDialog.Builder builder= new AlertDialog.Builder(activity);

        LayoutInflater inflater=activity.getLayoutInflater();

        builder.setView(inflater.inflate(R.layout.custom_dialog, null));
        builder.setCancelable(false);
        dialog=builder.create();
        dialog.show();

    }

    void dismissDialog(){
        dialog.dismiss();
    }
}
