package com.example.licenta;

import java.io.Serializable;

public class InformatiiSuplimentare implements Serializable {


    private String id;
    private String nume;
    private String prenume;
    private String numarTelefon;
    private float suprafataUtila;
    private String tipLocatar;
    private String idCont;
    private Boolean  areCentralaProprie;


    public InformatiiSuplimentare() {
    }

    public InformatiiSuplimentare(String id, String nume, String prenume, String numarTelefon, float suprafataUtila, String tipLocatar, String idCont,Boolean areCentralaProprie) {
        this.id = id;
        this.nume = nume;
        this.prenume = prenume;
        this.numarTelefon = numarTelefon;
        this.suprafataUtila = suprafataUtila;
        this.tipLocatar = tipLocatar;
        this.idCont = idCont;
        this.areCentralaProprie=areCentralaProprie;
    }


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getNume() {
        return nume;
    }

    public void setNume(String nume) {
        this.nume = nume;
    }

    public String getPrenume() {
        return prenume;
    }

    public void setPrenume(String prenume) {
        this.prenume = prenume;
    }

    public String getNumarTelefon() {
        return numarTelefon;
    }

    public void setNumarTelefon(String numarTelefon) {
        this.numarTelefon = numarTelefon;
    }

    public float getSuprafataUtila() {
        return suprafataUtila;
    }

    public void setSuprafataUtila(float suprafataUtila) {
        this.suprafataUtila = suprafataUtila;
    }

    public String getTipLocatar() {
        return tipLocatar;
    }

    public void setTipLocatar(String tipLocatar) {
        this.tipLocatar = tipLocatar;
    }

    public String getIdCont() {
        return idCont;
    }

    public void setIdCont(String idCont) {
        this.idCont = idCont;
    }

    public Boolean getAreCentralaProprie() {
        return areCentralaProprie;
    }

    public void setAreCentralaProprie(Boolean areCentralaProprie) {
        this.areCentralaProprie = areCentralaProprie;
    }

    @Override
    public String toString() {
        return "InformatiiSuplimentare{" +
                "id='" + id + '\'' +
                ", nume='" + nume + '\'' +
                ", prenume='" + prenume + '\'' +
                ", numarTelefon='" + numarTelefon + '\'' +
                ", suprafataUtila=" + suprafataUtila +
                ", tipLocatar='" + tipLocatar + '\'' +
                ", idCont='" + idCont + '\'' +
                '}';
    }
}
