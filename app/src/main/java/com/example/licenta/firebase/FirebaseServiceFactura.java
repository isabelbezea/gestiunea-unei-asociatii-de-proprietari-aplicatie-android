package com.example.licenta.firebase;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.licenta.Factura;
import com.example.licenta.Index;
import com.example.licenta.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class FirebaseServiceFactura {

    private static final String BILLS_TABLE="bills";
    private static FirebaseServiceFactura firebaseService;
    private DatabaseReference database;


    private FirebaseServiceFactura(){
        database= FirebaseDatabase.getInstance().getReference(BILLS_TABLE);

    }

    public static FirebaseServiceFactura getInstance(){
        if(firebaseService==null) {
            synchronized (FirebaseService.class) {
                if (firebaseService == null) {
                    firebaseService = new FirebaseServiceFactura();
                }
            }
        }
        return firebaseService;

    }

    public void  upsert(Factura factura){
        if(factura==null) return;

        if(factura.getId()==null || factura.getId().trim().isEmpty()){
            String id=database.push().getKey();
            factura.setId(id);}

        database.child(factura.getId()).setValue(factura);


    }

    public void delete(Factura factura){
        if(factura==null || factura.getId()==null||factura.getId().trim().isEmpty()) return;

        database.child(factura.getId()).removeValue();
        database.child(factura.getId()).removeEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Log.v("FirebaseService", "Remove is working");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.v("FirebaseService", "Remove is working");
            }
        });
    }

    public void attachDataChangeEventListener(final Callback<List<Factura>>  callback){
        //apelul metodelor upsert si delete de mai sus forteaza primirea unei notificari de la Firebase in acest eveniment


        //evenimentul este atasat la nivel de coaches (reference) prin urmare asculta orice modificare
        // de insert/update/delete executata asupra acestui nod

        database.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                List<Factura> bills=new ArrayList<>();
                for(DataSnapshot data: snapshot.getChildren()){
                    Factura factura=data.getValue(Factura.class);
                    if(factura!=null)
                    {
                        bills.add(factura);
                    }
                }
                callback.runResultOnUiThread(bills);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.v("FirebaseService", "Data is not available");
            }
        });
    }
}
