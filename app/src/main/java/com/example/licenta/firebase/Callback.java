package com.example.licenta.firebase;

public interface Callback<R> {
    void runResultOnUiThread(R result);
}
