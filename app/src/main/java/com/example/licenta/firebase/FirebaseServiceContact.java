package com.example.licenta.firebase;

import android.util.Log;

import androidx.annotation.NonNull;
import androidx.constraintlayout.widget.ConstraintLayout;

import com.example.licenta.Contact;
import com.example.licenta.FacturiAdapter;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class FirebaseServiceContact {

    private static final String CONTACTS_TABLE="contacts";
    private static FirebaseServiceContact firebaseService;
    private DatabaseReference database;


    private FirebaseServiceContact(){
        database= FirebaseDatabase.getInstance().getReference(CONTACTS_TABLE);

    }

    public static FirebaseServiceContact getInstance(){
        if(firebaseService==null) {
            synchronized (FirebaseService.class) {
                if (firebaseService == null) {
                    firebaseService = new FirebaseServiceContact();
                }
            }
        }
        return firebaseService;

    }

    public void  upsert(Contact contact){
        if(contact==null) return;

        if(contact.getId()==null || contact.getId().trim().isEmpty()){
            String id=database.push().getKey();
            contact.setId(id);}

        database.child(contact.getId()).setValue(contact);


    }

    public void delete(Contact contact){
        if(contact==null || contact.getId()==null||contact.getId().trim().isEmpty()) return;

        database.child(contact.getId()).removeValue();
        database.child(contact.getId()).removeEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Log.v("FirebaseService", "Remove is working");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.v("FirebaseService", "Remove is working");
            }
        });
    }

    public void attachDataChangeEventListener(final Callback<List<Contact>>  callback){
        //apelul metodelor upsert si delete de mai sus forteaza primirea unei notificari de la Firebase in acest eveniment


        //evenimentul este atasat la nivel de coaches (reference) prin urmare asculta orice modificare
        // de insert/update/delete executata asupra acestui nod

        database.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                List<Contact> contacts=new ArrayList<>();
                for(DataSnapshot data: snapshot.getChildren()){
                    Contact contact=data.getValue(Contact.class);
                    if(contact!=null)
                    {
                        contacts.add(contact);
                    }
                }
                callback.runResultOnUiThread(contacts);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.v("FirebaseService", "Data is not available");
            }
        });
    }

}
