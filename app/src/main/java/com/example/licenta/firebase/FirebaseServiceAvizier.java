package com.example.licenta.firebase;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.licenta.Avizier;
import com.example.licenta.Factura;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class FirebaseServiceAvizier {

    private static final String BILLS_TABLE="news";
    private static FirebaseServiceAvizier firebaseService;
    private DatabaseReference database;


    private FirebaseServiceAvizier(){
        database= FirebaseDatabase.getInstance().getReference(BILLS_TABLE);

    }

    public static FirebaseServiceAvizier getInstance(){
        if(firebaseService==null) {
            synchronized (FirebaseService.class) {
                if (firebaseService == null) {
                    firebaseService = new FirebaseServiceAvizier();
                }
            }
        }
        return firebaseService;

    }

    public void  upsert(Avizier avizier){
        if(avizier==null) return;

        if(avizier.getId()==null || avizier.getId().trim().isEmpty()){
            String id=database.push().getKey();
            avizier.setId(id);}

        database.child(avizier.getId()).setValue(avizier);


    }

    public void delete(Avizier avizier){
        if(avizier==null || avizier.getId()==null||avizier.getId().trim().isEmpty()) return;

        database.child(avizier.getId()).removeValue();
        database.child(avizier.getId()).removeEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Log.v("FirebaseService", "Remove is working");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.v("FirebaseService", "Remove is working");
            }
        });
    }

    public void attachDataChangeEventListener(final Callback<List<Avizier>>  callback){
        //apelul metodelor upsert si delete de mai sus forteaza primirea unei notificari de la Firebase in acest eveniment


        //evenimentul este atasat la nivel de coaches (reference) prin urmare asculta orice modificare
        // de insert/update/delete executata asupra acestui nod

        database.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                List<Avizier> news=new ArrayList<>();
                for(DataSnapshot data: snapshot.getChildren()){
                    Avizier avizier=data.getValue(Avizier.class);
                    if(avizier!=null)
                    {
                        news.add(avizier);
                    }
                }
                callback.runResultOnUiThread(news);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.v("FirebaseService", "Data is not available");
            }
        });
    }
}
