package com.example.licenta.firebase;


import android.util.Log;

import androidx.annotation.NonNull;

import com.example.licenta.InformatiiSuplimentare;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class FirebaseServiceInformatii {


    private static final String INFORMATIONS_TABLE="additionalInformations";
    private static FirebaseServiceInformatii firebaseService;
    private DatabaseReference database;


    private FirebaseServiceInformatii(){
        database= FirebaseDatabase.getInstance().getReference(INFORMATIONS_TABLE);

    }

    public static FirebaseServiceInformatii getInstance(){
        if(firebaseService==null) {
            synchronized (FirebaseService.class) {
                if (firebaseService == null) {
                    firebaseService = new FirebaseServiceInformatii();
                }
            }
        }
        return firebaseService;

    }

    public void  upsert(InformatiiSuplimentare informatiiSuplimentare){
        if(informatiiSuplimentare==null) return;

        if(informatiiSuplimentare.getId()==null || informatiiSuplimentare.getId().trim().isEmpty()){
            String id=database.push().getKey();
            informatiiSuplimentare.setId(id);}

        database.child(informatiiSuplimentare.getId()).setValue(informatiiSuplimentare);


    }

    public void delete(InformatiiSuplimentare informatiiSuplimentare){
        if(informatiiSuplimentare==null || informatiiSuplimentare.getId()==null||informatiiSuplimentare.getId().trim().isEmpty()) return;

        database.child(informatiiSuplimentare.getId()).removeValue();
        database.child(informatiiSuplimentare.getId()).removeEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Log.v("FirebaseService", "Remove is working");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.v("FirebaseService", "Remove is working");
            }
        });
    }

    public void attachDataChangeEventListener(final Callback<List<InformatiiSuplimentare>>  callback){
        //apelul metodelor upsert si delete de mai sus forteaza primirea unei notificari de la Firebase in acest eveniment


        //evenimentul este atasat la nivel de coaches (reference) prin urmare asculta orice modificare
        // de insert/update/delete executata asupra acestui nod

        database.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                List<InformatiiSuplimentare> informations=new ArrayList<>();
                for(DataSnapshot data: snapshot.getChildren()){
                    InformatiiSuplimentare informatiiSuplimentare=data.getValue(InformatiiSuplimentare.class);
                    if(informatiiSuplimentare!=null)
                    {
                        informations.add(informatiiSuplimentare);
                    }
                }
                callback.runResultOnUiThread(informations);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.v("FirebaseService", "Data is not available");
            }
        });
    }

}
