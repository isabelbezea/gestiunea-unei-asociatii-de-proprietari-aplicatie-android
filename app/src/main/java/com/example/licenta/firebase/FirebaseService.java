package com.example.licenta.firebase;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.licenta.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class FirebaseService {

    private static final String USERS_TABLE="users";
    private static FirebaseService firebaseService;
    private DatabaseReference database;


    private FirebaseService(){
        database= FirebaseDatabase.getInstance().getReference(USERS_TABLE);

    }

    public static FirebaseService getInstance(){
        if(firebaseService==null) {
            synchronized (FirebaseService.class) {
                if (firebaseService == null) {
                    firebaseService = new FirebaseService();
                }
            }
        }
        return firebaseService;

    }

    public void  upsert(User user){
        if(user==null) return;

        if(user.getId()==null || user.getId().trim().isEmpty()){
            String id=database.push().getKey();
            user.setId(id);}

        database.child(user.getId()).setValue(user);


    }

    public void delete(User user){
        if(user==null || user.getId()==null||user.getId().trim().isEmpty()) return;

        database.child(user.getId()).removeValue();
        database.child(user.getId()).removeEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Log.v("FirebaseService", "Remove is working");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.v("FirebaseService", "Remove is working");
            }
        });
    }

    public void attachDataChangeEventListener(final Callback<List<User>>  callback){
        //apelul metodelor upsert si delete de mai sus forteaza primirea unei notificari de la Firebase in acest eveniment


        //evenimentul este atasat la nivel de coaches (reference) prin urmare asculta orice modificare
        // de insert/update/delete executata asupra acestui nod

        database.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                List<User> users=new ArrayList<>();
                for(DataSnapshot data: snapshot.getChildren()){
                    User user=data.getValue(User.class);
                    if(user!=null)
                    {
                        users.add(user);
                    }
                }
                callback.runResultOnUiThread(users);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.v("FirebaseService", "Data is not available");
            }
        });
    }
}
