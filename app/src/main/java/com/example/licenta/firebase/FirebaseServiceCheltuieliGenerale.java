package com.example.licenta.firebase;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.licenta.Avizier;
import com.example.licenta.General;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class FirebaseServiceCheltuieliGenerale {


        private static final String SPENDS_TABLE="generalSpends";
        private static FirebaseServiceCheltuieliGenerale firebaseService;
        private DatabaseReference database;


        private FirebaseServiceCheltuieliGenerale(){
            database= FirebaseDatabase.getInstance().getReference(SPENDS_TABLE);

        }

        public static FirebaseServiceCheltuieliGenerale getInstance(){
            if(firebaseService==null) {
                synchronized (FirebaseService.class) {
                    if (firebaseService == null) {
                        firebaseService = new FirebaseServiceCheltuieliGenerale();
                    }
                }
            }
            return firebaseService;

        }

        public void  upsert(General general){
            if(general==null) return;

            if(general.getId()==null || general.getId().trim().isEmpty()){
                String id=database.push().getKey();
                general.setId(id);}

            database.child(general.getId()).setValue(general);


        }

        public void delete(General general){
            if(general==null || general.getId()==null||general.getId().trim().isEmpty()) return;

            database.child(general.getId()).removeValue();
            database.child(general.getId()).removeEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    Log.v("FirebaseService", "Remove is working");
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Log.v("FirebaseService", "Remove is working");
                }
            });
        }

        public void attachDataChangeEventListener(final Callback<List<General>>  callback){
            //apelul metodelor upsert si delete de mai sus forteaza primirea unei notificari de la Firebase in acest eveniment


            //evenimentul este atasat la nivel de coaches (reference) prin urmare asculta orice modificare
            // de insert/update/delete executata asupra acestui nod

            database.addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(@NonNull DataSnapshot snapshot) {
                    List<General> spends=new ArrayList<>();
                    for(DataSnapshot data: snapshot.getChildren()){
                        General general=data.getValue(General.class);
                        if(general!=null)
                        {
                            spends.add(general);
                        }
                    }
                    callback.runResultOnUiThread(spends);
                }

                @Override
                public void onCancelled(@NonNull DatabaseError error) {
                    Log.v("FirebaseService", "Data is not available");
                }
            });
        }
    }



