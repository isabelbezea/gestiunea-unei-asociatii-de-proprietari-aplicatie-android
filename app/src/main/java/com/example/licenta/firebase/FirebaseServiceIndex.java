package com.example.licenta.firebase;

import android.util.Log;

import androidx.annotation.NonNull;

import com.example.licenta.Index;
import com.example.licenta.User;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

public class FirebaseServiceIndex {

    private static final String INDEX_TABLE="index";
    private static FirebaseServiceIndex firebaseService;
    private DatabaseReference database;


    private FirebaseServiceIndex(){
        database= FirebaseDatabase.getInstance().getReference(INDEX_TABLE);

    }

    public static FirebaseServiceIndex getInstance(){
        if(firebaseService==null) {
            synchronized (FirebaseService.class) {
                if (firebaseService == null) {
                    firebaseService = new FirebaseServiceIndex();
                }
            }
        }
        return firebaseService;

    }

    public void  upsert(Index index){
        if(index==null) return;

        if(index.getId()==null || index.getId().trim().isEmpty()){
            String id=database.push().getKey();
            index.setId(id);}

        database.child(index.getId()).setValue(index);


    }

    public void delete(Index index){
        if(index==null || index.getId()==null||index.getId().trim().isEmpty()) return;

        database.child(index.getId()).removeValue();
        database.child(index.getId()).removeEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                Log.v("FirebaseService", "Remove is working");
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.v("FirebaseService", "Remove is working");
            }
        });
    }

    public void attachDataChangeEventListener(final Callback<List<Index>>  callback){
        //apelul metodelor upsert si delete de mai sus forteaza primirea unei notificari de la Firebase in acest eveniment


        //evenimentul este atasat la nivel de coaches (reference) prin urmare asculta orice modificare
        // de insert/update/delete executata asupra acestui nod

        database.addValueEventListener(new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot snapshot) {
                List<Index> indexes=new ArrayList<>();
                for(DataSnapshot data: snapshot.getChildren()){
                    Index index=data.getValue(Index.class);
                    if(index!=null)
                    {
                        indexes.add(index);
                    }
                }
                callback.runResultOnUiThread(indexes);
            }

            @Override
            public void onCancelled(@NonNull DatabaseError error) {
                Log.v("FirebaseService", "Data is not available");
            }
        });
    }
}
