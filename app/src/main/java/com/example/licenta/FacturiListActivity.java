package com.example.licenta;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;

import com.example.licenta.firebase.Callback;
import com.example.licenta.firebase.FirebaseService;
import com.example.licenta.firebase.FirebaseServiceCheltuieliGenerale;
import com.example.licenta.firebase.FirebaseServiceFactura;
import com.example.licenta.firebase.FirebaseServiceIndex;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

public class FacturiListActivity extends AppCompatActivity {


    FirebaseServiceFactura firebaseServiceFactura;
    FirebaseServiceCheltuieliGenerale firebaseServiceCheltuieliGenerale;


    private ListView lvFacturi;
    public FacturiAdapter facturiAdapter;
    public static ArrayList<Factura> facturi_list = new ArrayList<>();
    public static ArrayList<Factura> facturi_list2 = new ArrayList<>();
    public static ArrayList<General> spends_list = new ArrayList<>();


    public static ArrayList<Index> index_list = new ArrayList<>();

    public static float curatenie ;
   public static float diverse ;
    public static  float enel;
    public static float gaze ;
    public static float lift;
    public static float platiAdmnistrare ;
    public static float apaCalda;
    public static float apaRece;


    public static float difCald;
    public  static  float  difRece;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_facturi_list);

        lvFacturi = findViewById(R.id.lv_facturi);

        addAdapter();
        Log.v("pentrunoi", "CE");

        firebaseServiceFactura = FirebaseServiceFactura.getInstance();
        firebaseServiceFactura.attachDataChangeEventListener(dataChangeEventCallback());

        firebaseServiceCheltuieliGenerale = FirebaseServiceCheltuieliGenerale.getInstance();
        firebaseServiceCheltuieliGenerale.attachDataChangeEventListener(dataChangeEventCallback2());

        Log.v("pentrunoi", spends_list.size()+":((");
        Log.v("pentrunoi", facturi_list.size()+":(((");



//        FirebaseDatabase database=FirebaseDatabase.getInstance();
//        DatabaseReference myRef= database.getReference("spends");
//
//        myRef.addValueEventListener(new ValueEventListener() {
//            @Override
//            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
//                curatenie = Float.parseFloat(dataSnapshot.child("CURATENIE").getValue().toString());
//                diverse = Float.parseFloat(dataSnapshot.child("DIVERSE").getValue().toString());
//                enel = Float.parseFloat(dataSnapshot.child("ENEL").getValue().toString());
//                gaze = Float.parseFloat(dataSnapshot.child("GAZE").getValue().toString());
//                lift = Float.parseFloat(dataSnapshot.child("LIFT").getValue().toString());
//                platiAdmnistrare = Float.parseFloat(dataSnapshot.child("PLATI ADMINISTRARE").getValue().toString());
//                apaCalda=Float.parseFloat(dataSnapshot.child("APA CALDA").getValue().toString());
//                apaRece=Float.parseFloat(dataSnapshot.child("APA RECE").getValue().toString());
//
//
//                Log.v("pentrunoi","gaze in on create: "+gaze) ;
//
//
//
//            }
//
//            @Override
//            public void onCancelled(@NonNull DatabaseError error) {
//                Log.v("firebase_read", "failed to read value");
//            }
//        });



    }


    private Callback<List<Factura>> dataChangeEventCallback() {
        return new Callback<List<Factura>>() {
            @Override
            public void runResultOnUiThread(List<Factura> result) {
                if (result != null) {
                    facturi_list.clear();

                    facturi_list2.clear();


                    for (Factura factura : result) {

                        if (factura.getIdUtilizator().equals(LoginTabFragment.ID)) {

                            facturi_list.add(factura);
                        }
                        else if(LoginTabFragment.OKadministrator){
                            Log.v("pentrunoi", "vacuta");
                            if (factura.getIdUtilizator().equals(VizualizareDetaliiActivity.id)){
                                facturi_list2.add(factura);

                            }

                        }


//                    for (Factura factura:facturi_list){
//                        Log.v("pentrunoi", factura.toString());
//                        if(!factura.getIdUtilizator().equals(LoginTabFragment.ID)){
//                            Log.v("pentrunoi", factura.toString());
//                            facturi_list.remove(factura);
//                            Log.v("pentrunoi", "am eliminat");
//                            Log.v("pentrunoi", "count din data "+facturi_list.size());
//                        }
//                    }
                    }

                    Collections.reverse(facturi_list);
                    Collections.reverse(facturi_list2);
                    

                    Log.v("pentrunoi", "colectia: "+facturi_list);

                            notifyAdapter();
                        }
                    }

                }
                ;

            }


            private void notifyAdapter() {
                FacturiAdapter adapter = (FacturiAdapter) lvFacturi.getAdapter();
                adapter.notifyDataSetChanged();
            }

            private void addAdapter() {
                Log.v("pentrunoi", "!!!!!!!!!!!!!!" + LoginTabFragment.ID);
                Log.v("pentrunoi", "count" + facturi_list.size());

                if(LoginTabFragment.OKadministrator){
                    facturiAdapter = new FacturiAdapter(facturi_list2, this);

                }
                else{
                facturiAdapter = new FacturiAdapter(facturi_list, this);}

                lvFacturi.setAdapter(facturiAdapter);
            }


    private Callback<List<General>> dataChangeEventCallback2() {
        return new Callback<List<General>>() {
            @Override
            public void runResultOnUiThread(List<General> result) {
                if (result != null) {
                    spends_list.clear();
                    spends_list.addAll(result);

                }
            }

        };


    }

}