package com.example.licenta;

import androidx.appcompat.app.AppCompatActivity;

import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.icu.text.IDNA;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.licenta.firebase.Callback;

import com.example.licenta.firebase.FirebaseService;
import com.example.licenta.firebase.FirebaseServiceInformatii;


import java.util.ArrayList;
import java.util.List;

public class AdditionalInformationActivity extends AppCompatActivity {


    private FirebaseServiceInformatii firebaseService;
    private  static ArrayList<InformatiiSuplimentare> informations_list=new ArrayList<>() ;

    private FirebaseService firebaseServiceUser;
    private  static ArrayList<User> users_list=new ArrayList<>() ;


    int nrPersoaneSelectat;



    LinearLayout llTrimitere;
    EditText etNume;
    EditText etPrenume;
    EditText etNrTelefon;
    EditText etSuprafataUtila;
    RadioGroup rgTipLocatar;
    TextView tvActualizare;

    CheckBox cbCentrala;

    Spinner spnNrPersoane ;

    LinearLayout llDeconectare;
    LinearLayout llStergere;
    int ok=1;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_additional_information);


        llTrimitere=findViewById(R.id.ll_trimite_informatii);
        etNume=findViewById(R.id.et_nume);
        etNrTelefon=findViewById(R.id.et_numar_telefon);
        etSuprafataUtila=findViewById(R.id.et_mp);
        rgTipLocatar=findViewById(R.id.rg_tip_locatar);
        etPrenume=findViewById(R.id.et_prenume);
        cbCentrala=findViewById(R.id.cb_centrala);

        tvActualizare=findViewById(R.id.tv_actualizare);

        llDeconectare=findViewById(R.id.ll_deconectare);
        llStergere=findViewById(R.id.ll_stergere);


        llDeconectare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent mStartActivity = new Intent(AdditionalInformationActivity.this, IntroductoryActivity.class);
                int mPendingIntentId = 123456;
                PendingIntent mPendingIntent = PendingIntent.getActivity(AdditionalInformationActivity.this, mPendingIntentId,    mStartActivity, PendingIntent.FLAG_CANCEL_CURRENT);
                AlarmManager mgr = (AlarmManager)AdditionalInformationActivity.this.getSystemService(Context.ALARM_SERVICE);
                mgr.set(AlarmManager.RTC, System.currentTimeMillis(), mPendingIntent);
                System.exit(0); //you can also kill your app's process
            }
        });

        llStergere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for(User user: users_list) {
                    if (user.getId().equals(LoginTabFragment.ID)) {
                        firebaseServiceUser.delete(user);
                        Toast.makeText(AdditionalInformationActivity.this, "Cont sters cu succes!", Toast.LENGTH_SHORT).show();
                    }
                }

                for(InformatiiSuplimentare informatiiSuplimentare: informations_list){
                    if(informatiiSuplimentare.getIdCont().equals(LoginTabFragment.ID)){
                        firebaseService.delete(informatiiSuplimentare);
                    }
                }

                Intent intent=new Intent(AdditionalInformationActivity.this, IntroductoryActivity.class);
                startActivity(intent);
            }

        });




        RadioButton rbProprietar=findViewById(R.id.rb_proprietar);
        RadioButton rbChirias=findViewById(R.id.rb_chirias);



//        int selectedId= rgTipLocatar.getCheckedRadioButtonId();
//        if(selectedId!=-1)
//        {
//            rgTipLocatar.check(selectedId);
//        }


        firebaseService= FirebaseServiceInformatii.getInstance();
        setupListeners();
        firebaseService.attachDataChangeEventListener(dataChangeEventCallback());


        firebaseServiceUser= FirebaseService.getInstance();
        firebaseServiceUser.attachDataChangeEventListener(dataChangeEventCallback2());



        Log.v("pentrunoi", ":)"+Dashboard.prenume);
        if(LoginTabFragment.numeUtilizator!=null) {//adica daca deja exista informatiile completate
            etPrenume.setText(LoginTabFragment.numeUtilizator);
            etNume.setText(LoginTabFragment.prenumeUtilizator);
            etSuprafataUtila.setText(String.valueOf(LoginTabFragment.suprafataUtila));
            etNrTelefon.setText(LoginTabFragment.nrTelefon);
            if (LoginTabFragment.tipLocatar.equals("Proprietar")) {
                rbProprietar.setChecked(true);
            } else {
                rbChirias.setChecked(true);
            }

            if (LoginTabFragment.areCentralaProprie) {
                cbCentrala.setChecked(true);
            }
        }

        else{ ok=0;
        Log.v("pentrunoi", "m-am facut 0");}


    }

    private boolean validateContent() {
        if (etNume.getText().toString().isEmpty()) {
            Toast.makeText(this, "Numele trebuie introdus!", Toast.LENGTH_LONG).show();
            return false;
        }
        if (etPrenume.getText().toString().isEmpty()) {
            Toast.makeText(this, "Prenumele trebuie introdus!", Toast.LENGTH_LONG).show();
            return false;
        }
        if (etNrTelefon.getText().toString().isEmpty()) {
            Toast.makeText(this, "Numarul de telefon trebuie introdus!", Toast.LENGTH_LONG).show();
            return false;
        }
        if (etSuprafataUtila.getText().toString().isEmpty()) {
            Toast.makeText(this, "Suprafata utila a apartamentului trebuie introdusa!", Toast.LENGTH_LONG).show();
            return false;
        }

        if(rgTipLocatar.getCheckedRadioButtonId() == -1)
        {
            Toast.makeText(this, "Trebuie selectat tipul de locatar!", Toast.LENGTH_LONG).show();
            return  false;
        }


        return true;

    }


    private void setupListeners(){
        llTrimitere.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateContent()) {


                    if (ok == 0) {
                        InformatiiSuplimentare informatiiSuplimentare = new InformatiiSuplimentare();
                        informatiiSuplimentare.setNumarTelefon(etNrTelefon.getText().toString());
                        informatiiSuplimentare.setNume(etNume.getText().toString());
                        informatiiSuplimentare.setPrenume(etPrenume.getText().toString());
                        informatiiSuplimentare.setSuprafataUtila(Float.parseFloat(etSuprafataUtila.getText().toString()));

                        informatiiSuplimentare.setAreCentralaProprie(cbCentrala.isChecked());
                        Log.v("pentrunoi", "IDCONT: " + LoginTabFragment.ID);
                        informatiiSuplimentare.setIdCont(LoginTabFragment.ID);

                        int selectedId = rgTipLocatar.getCheckedRadioButtonId();
                        RadioButton rbSelected = findViewById(selectedId);
                        informatiiSuplimentare.setTipLocatar(rbSelected.getText().toString());

                        firebaseService.upsert(informatiiSuplimentare);
                        Toast.makeText(AdditionalInformationActivity.this, "Informatiile au fost trimise cu succes!", Toast.LENGTH_SHORT).show();

                    } else {
                        for (InformatiiSuplimentare informatiiSuplimentare : informations_list) {
                            if (informatiiSuplimentare.getIdCont().equals(LoginTabFragment.ID)) {
                                informatiiSuplimentare.setNumarTelefon(etNrTelefon.getText().toString());
                                informatiiSuplimentare.setNume(etNume.getText().toString());
                                informatiiSuplimentare.setPrenume(etPrenume.getText().toString());
                                informatiiSuplimentare.setSuprafataUtila(Float.parseFloat(etSuprafataUtila.getText().toString()));

                                informatiiSuplimentare.setAreCentralaProprie(cbCentrala.isChecked());
                                Log.v("pentrunoi", "IDCONT: " + LoginTabFragment.ID);
                                informatiiSuplimentare.setIdCont(LoginTabFragment.ID);

                                int selectedId = rgTipLocatar.getCheckedRadioButtonId();
                                RadioButton rbSelected = findViewById(selectedId);
                                informatiiSuplimentare.setTipLocatar(rbSelected.getText().toString());

                                firebaseService.upsert(informatiiSuplimentare);
                                Toast.makeText(AdditionalInformationActivity.this, "Informatiile au fost actualizate cu succes!", Toast.LENGTH_SHORT).show();
                            }
                        }
                    }
                }

                finish();
                Intent intent=new Intent(AdditionalInformationActivity.this, LoginActivity.class);
                startActivity(intent);

            }
        });


        tvActualizare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                openDialog2();
            }
        });
    }

    private Callback<List<InformatiiSuplimentare>> dataChangeEventCallback() {
        return new Callback<List<InformatiiSuplimentare>>() {
            @Override
            public void runResultOnUiThread(List<InformatiiSuplimentare> result) {
                if (result != null) {
                    informations_list.clear();
                    informations_list.addAll(result);

                }
            }

        };


    }


    private Callback<List<User>> dataChangeEventCallback2() {
        return new Callback<List<User>>() {
            @Override
            public void runResultOnUiThread(List<User> result) {
                if (result != null) {
                    users_list.clear();
                    users_list.addAll(result);

                }
            }

        };


    }


    public void openDialog2(){
        Dialog dialog=new Dialog(this);
        dialog.setContentView(R.layout.edit_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        ImageView ivTrimite=dialog.findViewById(R.id.iv_trimite_nr) ;


        spnNrPersoane=dialog.findViewById(R.id.sp_nr_persoane_edit);


        dialog.show();
        spnNrPersoane.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                nrPersoaneSelectat=Integer.parseInt(parent.getItemAtPosition(position).toString());
                Log.v("pentru noi", nrPersoaneSelectat+".");


            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });




        ivTrimite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {



                for(User user: users_list){
                    if(user.getId().equals(LoginTabFragment.ID)){


                        user.setNrPersoane(nrPersoaneSelectat);
                        firebaseServiceUser.upsert(user);
                    }
                }

                dialog.dismiss();
            }
        });

    }



}