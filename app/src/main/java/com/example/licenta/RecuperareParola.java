package com.example.licenta;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.licenta.firebase.Callback;
import com.example.licenta.firebase.FirebaseService;

import java.util.ArrayList;
import java.util.List;

public class RecuperareParola extends AppCompatActivity {

    EditText etUsernameR;
    EditText etEmailR;
    ImageView ivCerc;
    TextView tvIntrebareR;
    EditText etRaspunsR;
    ImageView ivConfirma;
    EditText etIntrodu;
    Button btnRecup;

    private  int ok = 0;

    FirebaseService firebaseService;
    public static ArrayList<User> users_list = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recuperare_parola);

        etUsernameR=findViewById(R.id.et_usernameR);
        etEmailR=findViewById(R.id.et_emailR);
        ivCerc=findViewById(R.id.iv_cerc);
        tvIntrebareR=findViewById(R.id.tv_intrebareR);
        etRaspunsR=findViewById(R.id.et_raspunsR);
        ivConfirma=findViewById(R.id.iv_confirma);
        etIntrodu=findViewById(R.id.et_introdu);
        btnRecup=findViewById(R.id.btn_recup);


        firebaseService=FirebaseService.getInstance();
        firebaseService.attachDataChangeEventListener(dataSelectEventCallback());

        ivCerc.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                String username = etUsernameR.getText().toString();
                String  email = etEmailR.getText().toString();

                for (final User user : users_list) {
                    if (username.equals(user.getNumeUtilizator()) && email.equals(user.getEmail())) {
                          ok = 1;
                        String intrebare=user.getIntrebareSecuritate();
                        tvIntrebareR.setVisibility(View.VISIBLE);
                        etRaspunsR.setVisibility(View.VISIBLE);
                        ivConfirma.setVisibility(View.VISIBLE);
                        tvIntrebareR.setText(String.valueOf(intrebare));


                        ivConfirma.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                String raspuns= etRaspunsR.getText().toString();
                                if(raspuns.equals(user.getRaspunsIntrebare())){
                                        etIntrodu.setVisibility(View.VISIBLE);
                                        btnRecup.setVisibility(View.VISIBLE);

                                        btnRecup.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                String nouaParola=etIntrodu.getText().toString();

                                                user.setParola(nouaParola);
                                                firebaseService.upsert(user);
                                                Toast.makeText(RecuperareParola.this,  "Parola a fost modificata cu succes!", Toast.LENGTH_SHORT).show();
                                                finish();


                                            }
                                        });

                                }
                                else {
                                    Toast.makeText(RecuperareParola.this,  "Raspunsul la intrebarea de securitate nu este corect!", Toast.LENGTH_SHORT).show();
                                }
                            }
                        });

                        break;
                    }

                }


            }
        });



    }
    private Callback<List<User>> dataSelectEventCallback() {
        return new Callback<List<User>>() {
            @Override
            public void runResultOnUiThread(List<User> result) {
                if (result != null) {
                    users_list.clear();
                    users_list.addAll(result);
                }
            }

        };
    }

}