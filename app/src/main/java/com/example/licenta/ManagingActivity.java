package com.example.licenta;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.SearchView;

import com.example.licenta.firebase.Callback;
import com.example.licenta.firebase.FirebaseService;
import com.example.licenta.firebase.FirebaseServiceFactura;
import com.example.licenta.firebase.FirebaseServiceInformatii;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class ManagingActivity extends AppCompatActivity {



    FirebaseServiceInformatii firebaseServiceInformatii;

    private ListView lvLocatari;
    public ManagingAdapter managingAdapter;
    public static ArrayList<InformatiiSuplimentare> informatiiSuplimentares_list = new ArrayList<>();



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_managing);

        lvLocatari=findViewById(R.id.lv_oameni);

        addAdapter();

        firebaseServiceInformatii =   FirebaseServiceInformatii.getInstance();
         firebaseServiceInformatii.attachDataChangeEventListener(dataChangeEventCallback());



         initSearchWidgets();


    }

    private Callback<List<InformatiiSuplimentare>> dataChangeEventCallback() {
        return new Callback<List<InformatiiSuplimentare>>() {
            @Override
            public void runResultOnUiThread(List<InformatiiSuplimentare> result) {
                if (result != null) {
                    informatiiSuplimentares_list.clear();
                    informatiiSuplimentares_list.addAll(result);
                        }



                    notifyAdapter();
                }

        }
                ;

    }

    private void notifyAdapter() {
        ManagingAdapter adapter = (ManagingAdapter) lvLocatari.getAdapter();
        adapter.notifyDataSetChanged();
    }

    private void addAdapter() {
        managingAdapter = new ManagingAdapter(informatiiSuplimentares_list, this);
        lvLocatari.setAdapter(managingAdapter);
    }




    private void initSearchWidgets(){

        SearchView searchView=(SearchView)findViewById(R.id.sv_search) ;

        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {


                Log.v("pentrunoi", "HIHI");

                ArrayList<InformatiiSuplimentare> informatiiSuplimentares_list2=new ArrayList<>();

                Log.v("pentrunoi", "size: "+informatiiSuplimentares_list.size());
                for(InformatiiSuplimentare obj : informatiiSuplimentares_list){
                    if(obj.getNume().toLowerCase().contains(newText.toLowerCase()) ||

                            obj.getPrenume().toLowerCase().contains(newText.toLowerCase())  ||
                            obj.getTipLocatar().toLowerCase().contains(newText.toLowerCase())

                    ){

                        Log.v("pentrunoi", "HIHI");
                        informatiiSuplimentares_list2.add(obj);
                    }
                }

                ManagingAdapter managingAdapter2= new ManagingAdapter( informatiiSuplimentares_list2, getApplicationContext());
                lvLocatari.setAdapter(managingAdapter2);


                return false;
            }
        });
    }
}