package com.example.licenta;

import java.io.Serializable;

public class Avizier implements Serializable {

    private String id;
    private String titlu;
    private String denumire;
    private int likes;
    private int dislikes;

    public Avizier() {
    }

    public Avizier(String id, String titlu, String denumire, int likes, int dislikes) {
        this.id = id;
        this.titlu = titlu;
        this.denumire = denumire;
        this.likes = likes;
        this.dislikes = dislikes;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitlu() {
        return titlu;
    }

    public void setTitlu(String titlu) {
        this.titlu = titlu;
    }

    public String getDenumire() {
        return denumire;
    }

    public void setDenumire(String denumire) {
        this.denumire = denumire;
    }

    public int getLikes() {
        return likes;
    }

    public void setLikes(int likes) {
        this.likes = likes;
    }

    public int getDislikes() {
        return dislikes;
    }

    public void setDislikes(int dislikes) {
        this.dislikes = dislikes;
    }

    @Override
    public String toString() {
        return "Avizier{" +
                "id='" + id + '\'' +
                ", titlu='" + titlu + '\'' +
                ", denumire='" + denumire + '\'' +
                ", likes=" + likes +
                ", dislikes=" + dislikes +
                '}';
    }
}
