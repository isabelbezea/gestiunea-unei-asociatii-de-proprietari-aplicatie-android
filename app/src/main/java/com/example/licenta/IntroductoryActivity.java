package com.example.licenta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.os.*;

import com.airbnb.lottie.LottieAnimationView;
import com.google.firebase.FirebaseApp;

public class IntroductoryActivity extends AppCompatActivity {

    private static int SPLASH_SCREEN=5000;

    ImageView mesaj;
    LottieAnimationView lottieAnimationView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_introductory);

        mesaj=findViewById(R.id.iv_mesaj);
        lottieAnimationView=findViewById(R.id.animatie);




        lottieAnimationView.animate().translationY(-1600).setDuration(1000).setStartDelay(4000);
        mesaj.animate().translationY(-1600).setDuration(1000).setStartDelay(4000);


        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                Intent intent = new Intent(IntroductoryActivity.this, LoginActivity.class);
                startActivity(intent);
             finish();
            }
        },SPLASH_SCREEN);

    }
}