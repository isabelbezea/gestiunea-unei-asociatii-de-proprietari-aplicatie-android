package com.example.licenta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.example.licenta.firebase.Callback;
import com.example.licenta.firebase.FirebaseServiceAvizier;
import com.example.licenta.firebase.FirebaseServiceFactura;

import java.util.ArrayList;
import java.util.List;

public class AdaugaAvizierActivity extends AppCompatActivity {

    EditText etTitlu;
    EditText etDescriere;
    Button btnTrimite;

    private  static ArrayList<Avizier> news_list=new ArrayList<>() ;

    private FirebaseServiceAvizier firebaseService;
    Avizier avizier=null;
    String titlu, descriere;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_adauga_avizier);

        etTitlu=findViewById(R.id.et_titlu_anunt);
        etDescriere=findViewById(R.id.et_descriere_anunt);
        btnTrimite=findViewById(R.id.btn_trimite) ;

        firebaseService= FirebaseServiceAvizier.getInstance();

        Intent incomingIntent =getIntent();
        titlu =incomingIntent.getStringExtra("titluOBIECT");
        descriere =incomingIntent.getStringExtra("descriereOBIECT");
        Log.v("pentrunoi", "obiectul: "+titlu);

        if(titlu!=null)
        {
            etTitlu.setText(titlu);
            etDescriere.setText(descriere);


        }
        setupListeners();
        firebaseService.attachDataChangeEventListener(dataChangeEventCallback());





    }
    private boolean validateContent() {
        if (etTitlu.getText().toString().isEmpty()) {
            Toast.makeText(this, "Titlul trebuie introdus!", Toast.LENGTH_LONG).show();
            return false;
        }
        if (etDescriere.getText().toString().isEmpty()) {
            Toast.makeText(this, "Descrierea trebuie introdusa!", Toast.LENGTH_LONG).show();
            return false;
        }


        return true;

    }
    private void setupListeners(){
        btnTrimite.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (validateContent()) {
                    if(titlu!=null){
                        Log.v("pentrunoi", "aici sunt");
                        for(Avizier avizier:news_list){
                            if(avizier.getTitlu().equals(titlu)){
                                Log.v("pentrunoi", avizier.toString());
                                avizier.setTitlu(etTitlu.getText().toString());
                                avizier.setDenumire(etDescriere.getText().toString());

                                firebaseService.upsert(avizier);

                               Log.v("pentrunoi", "aici sunt");
                            }
                        }
                    }else {

                        Avizier avizier = new Avizier();

                        avizier.setTitlu(etTitlu.getText().toString());
                        avizier.setDenumire(etDescriere.getText().toString());

                        avizier.setLikes(0);
                        avizier.setDislikes(0);


                   // Log.v("pentrunoi", avizier.toString());
                    Log.v("pentrunoi", "nu am inserat");
                    firebaseService.upsert(avizier);}





                    Intent intent = new Intent(AdaugaAvizierActivity.this, AvizierActivity.class);
                    startActivity(intent);

                }
            }
        });
    }



    private Callback<List<Avizier>> dataChangeEventCallback() {
        return new Callback<List<Avizier>>() {
            @Override
            public void runResultOnUiThread(List<Avizier> result) {
                if (result != null) {
                    news_list.clear();
                    news_list.addAll(result);

                }
            }

        };


    }


}