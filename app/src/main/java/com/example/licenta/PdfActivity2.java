package com.example.licenta;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.DownloadManager;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.example.licenta.firebase.Callback;
import com.example.licenta.firebase.FirebaseService;
import com.example.licenta.firebase.FirebaseServiceCheltuieliGenerale;
import com.example.licenta.firebase.FirebaseServiceFactura;
import com.example.licenta.firebase.FirebaseServiceInformatii;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.color.Color;
import com.itextpdf.kernel.color.DeviceRgb;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.border.Border;
import com.itextpdf.layout.border.SolidBorder;
import com.itextpdf.layout.element.Cell;
import com.itextpdf.layout.element.Image;
import com.itextpdf.layout.element.Paragraph;
import com.itextpdf.layout.element.Table;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

public class PdfActivity2 extends AppCompatActivity {

  Button btnVizualizeaza;
  Button btnInapoi;

    String pdfPath;


    General general=null;




    int ok=0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pdf2);

        btnVizualizeaza=findViewById(R.id.btn_vizualizeaza);
        btnInapoi=findViewById(R.id.btn_inapoi2);



        try{

            cratePdf();
        }
        catch (FileNotFoundException e){
            e.printStackTrace();
        }

        btnVizualizeaza.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

//                Uri uri= Uri.fromFile(file);


              Intent intent = new Intent(DownloadManager.ACTION_VIEW_DOWNLOADS);

//
//              intent.setDataAndType(uri, "application/pdf");
//                intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
//                intent.addFlags(Intent.FLAG_GRANT_WRITE_URI_PERMISSION);
//                 intent.setFlags(Intent. FLAG_ACTIVITY_CLEAR_TOP);

                startActivity(intent);
                finish();



            }
        });

        btnInapoi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(PdfActivity2.this, Dashboard.class);
                startActivity(intent);
            }
        });

    }



    private  void cratePdf() throws  FileNotFoundException{
        Log.v("pentrunoi", "iamhere0");

        pdfPath = Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DOWNLOADS).toString();

        Log.v("pentrunoi", "iamhere..........");

        String data_scadenta = FacturiAdapter.dataScadenta.split("/")[0]+"_"+FacturiAdapter.dataScadenta.split("/")[1];
        String denumire= "f_"+data_scadenta+".pdf";


Log.v("pentrunoi", "denumire: "+pdfPath+File.separator+denumire) ;
        File file=new File(pdfPath+File.separator +denumire);
        Log.v("pentrunoi", "iamhere..........");
        FileOutputStream outputStream=new FileOutputStream(file);


        Log.v("pentrunoi", "iamhere..........");


        PdfWriter writer = new PdfWriter(String.valueOf(file));
        PdfDocument pdfDocument = new PdfDocument(writer);
        Document document = new Document(pdfDocument);
        Log.v("pentrunoi", "iamhere0.1");

        document.setMargins(0,0,0,0);

        Drawable d1= getDrawable(R.drawable.white_background) ;
        Bitmap  bitmap1=((BitmapDrawable)d1).getBitmap();
        ByteArrayOutputStream stream1=new ByteArrayOutputStream();
        bitmap1.compress(Bitmap.CompressFormat.PNG,100, stream1 );
        byte[] bitmapData1=stream1.toByteArray();

        ImageData imageData1= ImageDataFactory.create(bitmapData1);
        Image image1=new Image(imageData1);


        Drawable d2= getDrawable(R.drawable.factura3) ;
        Bitmap  bitmap2=((BitmapDrawable)d2).getBitmap();
        ByteArrayOutputStream stream2=new ByteArrayOutputStream();
        bitmap2.compress(Bitmap.CompressFormat.PNG,50, stream2);
        byte[] bitmapData2=stream2.toByteArray();

        ImageData imageData2= ImageDataFactory.create(bitmapData2);
        Image image2=new Image(imageData2);

        Drawable d3= getDrawable(R.drawable.semnatura) ;
        Bitmap  bitmap3=((BitmapDrawable)d3).getBitmap();
        ByteArrayOutputStream stream3=new ByteArrayOutputStream();
        bitmap3.compress(Bitmap.CompressFormat.PNG,100, stream3);
        byte[] bitmapData3=stream3.toByteArray();

        ImageData imageData3= ImageDataFactory.create(bitmapData3);
        Image image3=new Image(imageData3);
        Log.v("pentrunoi", "iamhere0.2");

        float columnWidth1[]={120,220,120,100};
        Table table1 = new Table(columnWidth1);
        table1.setMarginLeft(20);
        table1.setMarginRight(20);


        String dataFactura;
        dataFactura=FacturiAdapter.dataScadenta.split("/")[1]+"/"+FacturiAdapter.dataScadenta.split("/")[2];

//        Log.v("pentrunoi", "datafactura:" + dataFactura);
//        Log.v("pentrunoi", "size spends_list:" + spends_list.size());
//
//        for(General obj : spends_list){
//            if(obj.getData().equals(dataFactura)){
//                Log.v("pentrunoi", "am intrat aiciiiiii");
//                general=obj;
//            }
//        }


        Log.v("pentrunoi", "aici: "+FacturiAdapter.generalTransmis.toString());

        Log.v("pentrunoi", "iamhere1");
        table1.addCell(new Cell().add(new Paragraph("Adresa de mail locatar").setFontSize(15).setFontColor(Color.BLACK)).setBorder(Border.NO_BORDER));
        table1.addCell(new Cell().add(new Paragraph(LoginTabFragment.email).setFontSize(15).setFontColor(Color.BLACK)).setBorder(Border.NO_BORDER));
        table1.addCell(new Cell().add(new Paragraph("Numar apartament").setFontSize(15).setFontColor(Color.BLACK)).setBorder(Border.NO_BORDER));
        table1.addCell(new Cell().add(new Paragraph(LoginTabFragment.nrApartament+"").setFontSize(15).setFontColor(Color.BLACK)).setBorder(Border.NO_BORDER));
        table1.addCell(new Cell().add(new Paragraph("ID Factura").setFontSize(15).setFontColor(Color.BLACK)).setBorder(Border.NO_BORDER));
        table1.addCell(new Cell().add(new Paragraph(FacturiAdapter.idFactura).setFontSize(15).setFontColor(Color.BLACK)).setBorder(Border.NO_BORDER));
        table1.addCell(new Cell().add(new Paragraph("Data scadenta").setFontSize(15).setFontColor(Color.BLACK)).setBorder(Border.NO_BORDER));
        table1.addCell(new Cell().add(new Paragraph(FacturiAdapter.dataScadenta).setFontSize(15).setFontColor(Color.BLACK)).setBorder(Border.NO_BORDER));


        Log.v("pentrunoi", "iamhere2");

        DeviceRgb secColor=new DeviceRgb(15,116, 186);
        float columnWidth2[]={220,100,100,120,  120};
        Table table2=new Table(columnWidth2);
        table2.setMargin(20);
        table2.addCell(new Cell().add(new Paragraph("Denumirea serviciilor").setFontColor(Color.WHITE).setBorder(new SolidBorder(secColor, 1)).setBackgroundColor(secColor)));
        table2.addCell(new Cell().add(new Paragraph("U.M.").setFontColor(Color.WHITE).setBorder(new SolidBorder(secColor, 1)).setBackgroundColor(secColor)));
        table2.addCell(new Cell().add(new Paragraph("Cantitatea").setFontColor(Color.WHITE).setBorder(new SolidBorder(secColor, 1)).setBackgroundColor(secColor)));
        table2.addCell(new Cell().add(new Paragraph("Pret unitar -lei-").setFontColor(Color.WHITE).setBorder(new SolidBorder(secColor, 1)).setBackgroundColor(secColor)));
        table2.addCell(new Cell().add(new Paragraph("Valoarea -lei-").setFontColor(Color.WHITE).setBorder(new SolidBorder(secColor, 1)).setBackgroundColor(secColor)));

        table2.addCell(new Cell().add(new Paragraph("Gaze naturale")));
        table2.addCell(new Cell().add(new Paragraph("pers")));
        table2.addCell(new Cell().add(new Paragraph(LoginTabFragment.nrPersoane+"")));
        table2.addCell(new Cell().add(new Paragraph(FacturiAdapter.generalTransmis.getGazeNaturale()+"")));
        table2.addCell(new Cell().add(new Paragraph(FacturiAdapter.generalTransmis.getGazeNaturale()*LoginTabFragment.nrPersoane+"")));

        table2.addCell(new Cell().add(new Paragraph("Energie electrica")));
        table2.addCell(new Cell().add(new Paragraph("pers")));
        table2.addCell(new Cell().add(new Paragraph(LoginTabFragment.nrPersoane+"")));
        table2.addCell(new Cell().add(new Paragraph(FacturiAdapter.generalTransmis.getEnergieElectrica()+"")));
        table2.addCell(new Cell().add(new Paragraph(FacturiAdapter.generalTransmis.getEnergieElectrica()*LoginTabFragment.nrPersoane+"")));

        table2.addCell(new Cell().add(new Paragraph("Deseuri menajere")));
        table2.addCell(new Cell().add(new Paragraph("pers")));
        table2.addCell(new Cell().add(new Paragraph(LoginTabFragment.nrPersoane+"")));
        table2.addCell(new Cell().add(new Paragraph(FacturiAdapter.generalTransmis.getDeseuriMenajere()+"")));
        table2.addCell(new Cell().add(new Paragraph(FacturiAdapter.generalTransmis.getDeseuriMenajere()*LoginTabFragment.nrPersoane+"")));

        table2.addCell(new Cell().add(new Paragraph("Apa calda")));
        table2.addCell(new Cell().add(new Paragraph("mc")));
        table2.addCell(new Cell().add(new Paragraph(FacturiAdapter.difCald+"")));
        table2.addCell(new Cell().add(new Paragraph(FacturiAdapter.generalTransmis.getApaCalda() + "")));
        table2.addCell(new Cell().add(new Paragraph(FacturiAdapter.generalTransmis.getApaCalda() * FacturiAdapter.difCald + "")));

        table2.addCell(new Cell().add(new Paragraph("Apa rece")));
        table2.addCell(new Cell().add(new Paragraph("mc")));
        table2.addCell(new Cell().add(new Paragraph(FacturiAdapter.difRece+"")));
        table2.addCell(new Cell().add(new Paragraph(FacturiAdapter.generalTransmis.getApaRece() + "")));
        table2.addCell(new Cell().add(new Paragraph(FacturiAdapter.generalTransmis.getApaRece() * FacturiAdapter.difRece + "")));

        table2.addCell(new Cell().add(new Paragraph("Caldura")));
        table2.addCell(new Cell().add(new Paragraph("mp")));
        table2.addCell(new Cell().add(new Paragraph(LoginTabFragment.suprafataUtila+"")));
        table2.addCell(new Cell().add(new Paragraph(FacturiAdapter.generalTransmis.getCaldura()+"")));
        table2.addCell(new Cell().add(new Paragraph(FacturiAdapter.generalTransmis.getCaldura()*LoginTabFragment.suprafataUtila+"")));

        table2.addCell(new Cell().add(new Paragraph("Caldura parti comune")));
        table2.addCell(new Cell().add(new Paragraph("mp")));
        table2.addCell(new Cell().add(new Paragraph(LoginTabFragment.suprafataUtila+"")));
        table2.addCell(new Cell().add(new Paragraph(FacturiAdapter.generalTransmis.getCalduraPartiComune()+"")));
        table2.addCell(new Cell().add(new Paragraph(FacturiAdapter.generalTransmis.getCalduraPartiComune()*LoginTabFragment.suprafataUtila+"")));

        table2.addCell(new Cell().add(new Paragraph("Apa pluviala")));
        table2.addCell(new Cell().add(new Paragraph("mp")));
        table2.addCell(new Cell().add(new Paragraph(LoginTabFragment.suprafataUtila+"")));
        table2.addCell(new Cell().add(new Paragraph(FacturiAdapter.generalTransmis.getApaPluviala()+"")));
        table2.addCell(new Cell().add(new Paragraph(FacturiAdapter.generalTransmis.getApaPluviala()*LoginTabFragment.suprafataUtila+"")));

        table2.addCell(new Cell().add(new Paragraph("Servicii curatenie")));
        table2.addCell(new Cell().add(new Paragraph("mp")));
        table2.addCell(new Cell().add(new Paragraph(LoginTabFragment.suprafataUtila+"")));
        table2.addCell(new Cell().add(new Paragraph(FacturiAdapter.generalTransmis.getServiciiCuratenie()+"")));
        table2.addCell(new Cell().add(new Paragraph(FacturiAdapter.generalTransmis.getServiciiCuratenie()*LoginTabFragment.suprafataUtila+"")));

        table2.addCell(new Cell().add(new Paragraph("Servicii contabilitate")));
        table2.addCell(new Cell().add(new Paragraph("mp")));
        table2.addCell(new Cell().add(new Paragraph(LoginTabFragment.suprafataUtila+"")));
        table2.addCell(new Cell().add(new Paragraph(FacturiAdapter.generalTransmis.getServiciiContabilitate()+"")));
        table2.addCell(new Cell().add(new Paragraph(FacturiAdapter.generalTransmis.getServiciiContabilitate()*LoginTabFragment.suprafataUtila+"")));

        table2.addCell(new Cell().add(new Paragraph("Contract mandat presedinte")));
        table2.addCell(new Cell().add(new Paragraph("mp")));
        table2.addCell(new Cell().add(new Paragraph(LoginTabFragment.suprafataUtila+"")));
        table2.addCell(new Cell().add(new Paragraph(FacturiAdapter.generalTransmis.getMandat()+"")));
        table2.addCell(new Cell().add(new Paragraph(FacturiAdapter.generalTransmis.getMandat()*LoginTabFragment.suprafataUtila+"")));






        table2.addCell(new Cell(1,3).add(new Paragraph("")));
        table2.addCell(new Cell().add(new Paragraph("TOTAL").setFontColor(Color.WHITE).setBorder(new SolidBorder(secColor, 1)).setBackgroundColor(secColor)));
        table2.addCell(new Cell().add(new Paragraph(FacturiAdapter.sumaDePlata).setFontColor(Color.WHITE).setBorder(new SolidBorder(secColor, 1)).setBackgroundColor(secColor)));

        table2.addCell(new Cell(1,5).add(new Paragraph("")).setBorder(Border.NO_BORDER));
        table2.addCell(new Cell(1,5).add(new Paragraph("")).setBorder(Border.NO_BORDER));
        table2.addCell(new Cell(1,5).add(new Paragraph("")).setBorder(Border.NO_BORDER));
        table2.addCell(new Cell(1,5).add(new Paragraph("")).setBorder(Border.NO_BORDER));
        table2.addCell(new Cell(1,5).add(new Paragraph("")).setBorder(Border.NO_BORDER));

image3.setWidthPercent(30);
        table2.addCell(new Cell().add(new Paragraph("Data emiterii: ").setFontSize(15).setFontColor(Color.BLACK)).setBorder(Border.NO_BORDER));
        table2.addCell(new Cell().add(new Paragraph(giveDate()).setFontSize(15).setFontColor(Color.BLACK)).setBorder(Border.NO_BORDER));
        table2.addCell(new Cell().add(new Paragraph(" Semnatura administrator: ").setFontSize(15).setFontColor(Color.BLACK)).setBorder(Border.NO_BORDER));




        document.add(image1.setFixedPosition(0,0));
     document.add(image3.setFixedPosition( 390,50));


        document.add(image2);

        document.add(table1);
        document.add(table2);


        document.close();
//        Toast.makeText(this, "Pdf Created", Toast.LENGTH_SHORT).show();




    }

    public String giveDate() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
        return sdf.format(cal.getTime());
    }




}