package com.example.licenta;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.airbnb.lottie.LottieAnimationView;
import com.example.licenta.firebase.Callback;
import com.example.licenta.firebase.FirebaseServiceFactura;
import com.example.licenta.firebase.FirebaseServiceInformatii;
import com.github.mikephil.charting.utils.ColorTemplate;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.util.ArrayList;
import java.util.List;

public class  Dashboard extends AppCompatActivity {

    LottieAnimationView animation;

    LinearLayout llIntroducere;

    LinearLayout llFacturi;

    LinearLayout llAvizier;

    LinearLayout llDocumente;

    LinearLayout llStatistici;

    LinearLayout llLegislatie;

    LottieAnimationView animationView;

    public static ArrayList<Factura> facturi_list = new ArrayList<>();
    FirebaseServiceFactura firebaseServiceFactura;


    private FirebaseServiceInformatii firebaseService;
    private  static ArrayList<InformatiiSuplimentare> informations_list=new ArrayList<>() ;

    public static String nume;
    public static String prenume;
    public static String nrTelefon;
    public static float suprafataUtila;






     @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);

        animationView=findViewById(R.id.user_animation);

        llIntroducere=findViewById(R.id.ll_introducere);

        llFacturi=findViewById(R.id.ll_facturi);

        llAvizier=findViewById(R.id.ll_avizier);

        llDocumente=findViewById(R.id.ll_documente);

        llStatistici=findViewById(R.id.ll_statistici);

        llLegislatie=findViewById(R.id.ll_legislatie);


        firebaseServiceFactura = FirebaseServiceFactura.getInstance();
        firebaseServiceFactura.attachDataChangeEventListener(dataChangeEventCallback());

        firebaseService= FirebaseServiceInformatii.getInstance();
        firebaseService.attachDataChangeEventListener(dataChangeEventCallback2());

         TextView tvHello=findViewById(R.id.tv_hello);
         TextView tvAddress=findViewById(R.id.tv_address);

         if(LoginTabFragment.numeUtilizator!=null){
         tvHello.setText("Bine ai venit, "+LoginTabFragment.numeUtilizator);}
         else{
             tvHello.setText("Bine ai venit!");

         }
         tvAddress.setText("Apartament " + LoginTabFragment.nrApartament);


         if(LoginTabFragment.numeUtilizator==null){
             openDialog();
         }


    retrieve();
    animationView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent(Dashboard.this, AdditionalInformationActivity.class);
                startActivity(intent);
            }
        });

         llIntroducere.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 Intent intent=new Intent(Dashboard.this, IntroducereActivity.class);
                 startActivity(intent);
             }
         });

        llFacturi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (Factura factura: facturi_list){
                    Log.v("pentrunoi","nooo: "+factura.toString());
                }
                if (facturi_list.size() == 0) {
                    Log.v("pentrunoi", "am intrat aici");
                    Intent intent = new Intent(Dashboard.this, FacturiInexistenteActivity.class);
                    startActivity(intent);

                } else {

                    Intent intent = new Intent(Dashboard.this, FacturiListActivity.class);
                    startActivity(intent);
                }
            }
        });


        llAvizier.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Dashboard.this, AvizierActivity.class);
                startActivity(intent);
            }
        });



        llDocumente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Dashboard.this, DocumentActivity.class);
                startActivity(intent);
            }
        });


        llStatistici.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(Dashboard.this, StatisticiActivity.class);
                startActivity(intent);
            }
        });

         llLegislatie.setOnClickListener(new View.OnClickListener() {
             @Override
             public void onClick(View v) {
                 Intent i = new Intent(
                         Intent.ACTION_VIEW,
                         Uri.parse("http://legislatie.just.ro/Public/DetaliiDocument/203233")
                 );
                 startActivity(i);
             }
         });






         writeToFirebaseDatabase();


    }
    private void writeToFirebaseDatabase(){
        FirebaseDatabase database=FirebaseDatabase.getInstance();
        DatabaseReference myRef= database.getReference("spends");


        myRef.child("LIFT").setValue("14");
        myRef.child("CURATENIE").setValue("11");
        myRef.child("ENEL").setValue("5");
        myRef.child("GAZE").setValue("30");
        myRef.child("LIFT").setValue("14");
        myRef.child("PLATI ADMINISTRARE").setValue("21");
        myRef.child("DIVERSE").setValue("15");
        myRef.child("APA CALDA").setValue("11");
        myRef.child("APA RECE").setValue("8");




    }

    private Callback<List<Factura>> dataChangeEventCallback() {
        return new Callback<List<Factura>>() {
            @Override
            public void runResultOnUiThread(List<Factura> result) {
                if (result != null) {
                    facturi_list.clear();

                    for (Factura factura : result) {

                        if (factura.getIdUtilizator().equals(LoginTabFragment.ID)) {

                            facturi_list.add(factura);
                        }
                    }

                }
            }

        }
                ;

    }

    private Callback<List<InformatiiSuplimentare>> dataChangeEventCallback2() {
        return new Callback<List<InformatiiSuplimentare>>() {
            @Override
            public void runResultOnUiThread(List<InformatiiSuplimentare> result) {
                if (result != null) {
                    informations_list.clear();
                    informations_list.addAll(result);

                }
            }

        };


    }


    public void retrieve(){
        Log.v("pentrunoi", "size: "+informations_list.size());
        for(InformatiiSuplimentare informatiiSuplimentare: informations_list)
        {
            if(informatiiSuplimentare.getIdCont().equals(LoginTabFragment.ID))
            {
                if(informatiiSuplimentare.getSuprafataUtila()!=0){


                    prenume=informatiiSuplimentare.getPrenume();
                    nume=informatiiSuplimentare.getNume();
                    nrTelefon=informatiiSuplimentare.getNumarTelefon();
                    suprafataUtila=informatiiSuplimentare.getSuprafataUtila();


                }
            }
        }


    }


    public void openDialog(){
        Dialog dialog=new Dialog(this);
        dialog.setContentView(R.layout.reminder_dialog);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Button btnCancel=dialog.findViewById(R.id.btn_cancel) ;
        Button btnOK=dialog.findViewById(R.id.btn_ok) ;

        dialog.show();

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });

        btnOK.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent intent=new Intent(Dashboard.this, AdditionalInformationActivity.class);
                startActivity(intent);

            }
        });

    }




}