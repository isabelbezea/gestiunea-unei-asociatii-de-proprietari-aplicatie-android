package com.example.licenta;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

public class DocumentActivity extends AppCompatActivity {

    TextView tvIncarca;

    LinearLayout llActeAsociatie;
    LinearLayout llContracte;
    LinearLayout llProceseVerbale;

    LinearLayout llFacturi;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_document);

        tvIncarca=findViewById(R.id.tv_incarca);

        llActeAsociatie=findViewById(R.id.ll_acte_asociatie);

        llContracte=findViewById(R.id.ll_contracte);
        
        llProceseVerbale=findViewById(R.id.ll_procese);

        llFacturi=findViewById(R.id.ll_facturi_furnizori);


        if(LoginTabFragment.OKadministrator){
            tvIncarca.setVisibility(View.VISIBLE);


            tvIncarca.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent=new Intent(DocumentActivity.this, IncarcaPDFActivity.class);
                    startActivity(intent);


                }
            });
        }

        llActeAsociatie.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent (DocumentActivity.this, DocumenteActeAsociatieActivity.class);
                startActivity(intent);
            }
        });

        llContracte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent (DocumentActivity.this, DocumenteContracteActivity.class);
                startActivity(intent);
            }
        });

        llProceseVerbale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent (DocumentActivity.this, DocumenteProceseVerbaleActivity.class);
                startActivity(intent);
            }
        });



        llFacturi.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent=new Intent (DocumentActivity.this, DocumenteFacturiActivity.class);
                startActivity(intent);
            }
        });




    }

    public void openDialog(){
            AddDocumentDialog addDocumentDialog=new AddDocumentDialog();
            addDocumentDialog.show(getSupportFragmentManager(), "Fisier PDF");

    }
}