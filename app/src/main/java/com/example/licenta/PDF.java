package com.example.licenta;

public class PDF {

    public String categorie;
    public String denumire;
    public String descriere;
    public String url;

    public PDF() {
    }

    public PDF(String categorie, String denumire, String descriere, String url) {
        this.categorie = categorie;
        this.denumire = denumire;
        this.descriere = descriere;
        this.url = url;
    }

    public String getCategorie() {
        return categorie;
    }

    public void setCategorie(String categorie) {
        this.categorie = categorie;
    }

    public String getDenumire() {
        return denumire;
    }

    public void setDenumire(String denumire) {
        this.denumire = denumire;
    }

    public String getDescriere() {
        return descriere;
    }

    public void setDescriere(String descriere) {
        this.descriere = descriere;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    @Override
    public String toString() {
        return "PDF{" +
                "categorie='" + categorie + '\'' +
                ", denumire='" + denumire + '\'' +
                ", descriere='" + descriere + '\'' +
                ", url='" + url + '\'' +
                '}';
    }
}
