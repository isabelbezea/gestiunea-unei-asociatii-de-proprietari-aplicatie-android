package com.example.licenta;

import androidx.appcompat.app.AppCompatActivity;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;

import com.github.mikephil.charting.charts.BarChart;
import com.github.mikephil.charting.data.BarData;
import com.github.mikephil.charting.data.BarDataSet;
import com.github.mikephil.charting.data.BarEntry;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.util.ArrayList;

public class GraficBareActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_grafic_bare);


        BarChart barChart= findViewById(R.id.grafic_bare1) ;

        ArrayList<BarEntry> visitors=new ArrayList<>();

//        for(Factura factura : facturi_list){
//            visitors.add(new BarEntry(Float.parseFloat(factura.getDataScadenta()),Float.parseFloat( factura.getSumaDePlata())));
//        }

        visitors.add(new BarEntry(Float.parseFloat("2014/2015"), 430));
        visitors.add(new BarEntry(Float.parseFloat("2015"),485));
        visitors.add(new BarEntry(2016,500));




        BarDataSet barDataSet=new BarDataSet(visitors, "Visitors");

        barDataSet.setColors(ColorTemplate.MATERIAL_COLORS);
        barDataSet.setValueTextColor(Color.BLACK);
        barDataSet.setValueTextSize(16f);

        BarData barData=new BarData(barDataSet);

        barChart.setFitBars(true);
        barChart.setData(barData);
        barChart.getDescription().setText("Grafic cu bare");
        barChart.animateY( 2000);


    }

    }
